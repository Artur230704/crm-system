package kg.attractor.esdp.mercsgroup.tests;

import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.entities.Request;
import kg.attractor.esdp.mercsgroup.entities.Status;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.OrderRepository;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CustomerProfileTest {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private OrderRepository orderRepository;

    @Test
    public void applyCustomerProfileChangesTest() {
        Customer customer = Customer.builder()
                .id(1L)
                .email("cr1@mail.ru")
                .name("old-name")
                .post("old-post")
                .number("old-number")
                .company("old-company")
                .build();

        CustomerDTO customerWithNewChanges = CustomerDTO.builder()
                .name("new-name")
                .post("new-post")
                .number("new-number")
                .company("new-company")
                .build();

        customerService.applyCustomerProfileChanges(customer,customerWithNewChanges);

        Assertions.assertEquals("new-name", customer.getName());
        Assertions.assertEquals("new-post", customer.getPost());
        Assertions.assertEquals("new-number", customer.getNumber());
        Assertions.assertEquals("new-company", customer.getCompany());

        verify(customerRepository, times(1)).save(customer);
    }

    @Test
    public void getCustomerOrdersTest(){
        var customer = Customer.builder()
                .id(1L)
                .build();
        var order1 = Order.builder()
                .build();
        var order2 = Order.builder()
                .build();
        List<Order> ordersList = Arrays.asList(order1, order2);
        Pageable pageable = PageRequest.of(0, 2);
        Page<Order> orders = new PageImpl<>(ordersList, pageable, ordersList.size());

        when(orderRepository.findOrdersByCustomerId(customer.getId(),pageable)).thenReturn(orders);

        var result = orderRepository.findOrdersByCustomerId(customer.getId(),pageable);

        Assertions.assertEquals(orders, result);
        Assertions.assertEquals(2, result.getTotalElements());
        Assertions.assertEquals(1, result.getTotalPages());
        Assertions.assertEquals(0, result.getNumber());
        Assertions.assertEquals(2, result.getSize());

        verify(orderRepository, times(1)).findOrdersByCustomerId(eq(customer.getId()),eq(pageable));
    }

    @Test
    public void confirmOrderTest(){
        var status = Status.builder()
                .id(1L)
                .status("В обработке")
                .build();

        var request = Request.builder()
                .id(1L)
                .build();
        var order = Order.builder()
                .id(1L)
                .status(status)
                .request(request)
                .build();
        String newStatus = "Ожидается подтверждение";
        orderService.confirmOrder(order, newStatus);

        Assertions.assertEquals(newStatus, order.getStatus().getStatus());

        verify(orderRepository, times(1)).save(order);
    }

    @Test
    public void searchOrdersTest(){
        var customer = Customer.builder()
                .id(1L)
                .build();
        var order1 = Order.builder()
                .build();
        var order2 = Order.builder()
                .build();
        List<Order> ordersList = Arrays.asList(order1, order2);
        Pageable pageable = PageRequest.of(0, 2);
        Page<Order> orders = new PageImpl<>(ordersList, pageable, ordersList.size());

        when(orderRepository.searchByProductAndDescriptionAndStatus(any(), any(),any(),any(),any())).thenReturn(orders);

        Optional<String> name = Optional.empty();
        Optional<String> description = Optional.empty();
        Optional<String> status = Optional.empty();

        var result = orderService.searchCustomerOrders(customer.getId(),0,2,name,description,status);

        Assertions.assertEquals(orders, result);
        Assertions.assertEquals(2, result.getTotalElements());
        Assertions.assertEquals(1, result.getTotalPages());
        Assertions.assertEquals(0, result.getNumber());
        Assertions.assertEquals(2, result.getSize());

        verify(orderRepository, times(1)).searchByProductAndDescriptionAndStatus(eq(name),eq(description),eq(status),eq(customer.getId()),eq(pageable));

    }

}
