package kg.attractor.esdp.mercsgroup.tests;

import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ManagerCustomerBlockTest {

    @Autowired
    private CustomerService customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void getCustomersListTest(){
        var customer1 = Customer.builder()
                .build();
        var customer2 = Customer.builder()
                .build();
        List<Customer> customerList = Arrays.asList(customer1, customer2);
        Pageable pageable = PageRequest.of(0, 2);
        Page<Customer> customers = new PageImpl<>(customerList, pageable, customerList.size());

        when(customerRepository.findCustomersBy(any())).thenReturn(customers);

        var result = customerService.getAllCustomersByPageable(0,2, "name", 1);

        Assertions.assertEquals(customers, result);
        Assertions.assertEquals(2, result.getTotalElements());
        Assertions.assertEquals(1, result.getTotalPages());
        Assertions.assertEquals(0, result.getNumber());
        Assertions.assertEquals(2, result.getSize());
    }

    @Test
    public void testGetAllCustomersByNameOrEmail() {
        Customer customer1 = Customer.builder()
                .build();
        Customer customer2 = Customer.builder()
                .build();

        List<Customer> customers = Arrays.asList(customer1, customer2);

        Pageable pageable = PageRequest.of(0, 10);
        when(customerRepository.searchByNameAndEmailAndPageable(any(), any(), eq(pageable)))
                .thenReturn(new PageImpl<>(customers));

        Optional<String> name = Optional.empty();
        Optional<String> email = Optional.empty();
        Page<Customer> result = customerService.getAllCustomersByNameOrEmail(0, 10, name, email);

        Assertions.assertEquals(customers, result.getContent());
        Assertions.assertEquals(2, result.getTotalElements());
        Assertions.assertEquals(1, result.getTotalPages());
        Assertions.assertEquals(0, result.getNumber());
        Assertions.assertEquals(2, result.getSize());

        verify(customerRepository, times(1)).searchByNameAndEmailAndPageable(eq(name), eq(email), eq(pageable));
    }

}
