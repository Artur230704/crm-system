package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.request.NewRequestDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Delivery;
import kg.attractor.esdp.mercsgroup.entities.Request;
import kg.attractor.esdp.mercsgroup.entities.Status;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.DeliveryRepository;
import kg.attractor.esdp.mercsgroup.repositories.RequestRepository;
import kg.attractor.esdp.mercsgroup.repositories.StatusRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
public class RequestServiceTest {

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private DeliveryRepository deliveryRepository;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private RequestRepository requestRepository;

    @Autowired
    private RequestService requestService;

    private static final String EMAIL = "cr1@mail.ru";

    @Test
    void addNewRequest_Successful() throws IOException {
        NewRequestDTO newRequestDto = TestUtil.createNewRequestDto();
        Customer customer = TestUtil.createCustomer(EMAIL);
        Delivery delivery = TestUtil.createDelivery();
        Status status = Status.builder()
                .id(1L)
                .status("Новый")
                .build();

        when(customerRepository.getCustomerById(newRequestDto.getCustomerId())).thenReturn(Optional.of(customer));
        when(deliveryRepository.findById(newRequestDto.getDeliveryId())).thenReturn(Optional.of(delivery));
        when(statusRepository.findStatusByStatus("Новый")).thenReturn(Optional.of(status));

        requestService.addNewRequest(newRequestDto);

        verify(requestRepository, times(1)).save(any());
    }

    @Test
    void addNewRequest_Unsuccessful() {
        NewRequestDTO newRequestDto = TestUtil.createNewRequestDto();

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> requestService.addNewRequest(newRequestDto));

        String expectedErrorMessage = "Customer not found for ID: " + newRequestDto.getCustomerId();
        assertEquals(expectedErrorMessage, illegalArgumentException.getMessage());
    }

    @Test
    void testGetCustomerRequests_GetSuccessful() {
        long customerId = 1L;
        int page = 0;
        int size = 5;
        List<Request> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Request request = TestUtil.createRequest(EMAIL);
            request.setId(i + 1L);
            list.add(request);
        }
        Pageable pageable = PageRequest.of(page, size);
        Page<Request> requestPage = new PageImpl<>(list, pageable, list.size());

        when(requestRepository.findRequestsByCustomerId(customerId, pageable)).thenReturn(requestPage);

        Page<Request> foundPage = requestService.getCustomerRequests(customerId, page, size);

        verify(requestRepository, times(1)).findRequestsByCustomerId(customerId, pageable);
        assertEquals(foundPage, requestPage);
    }

    @Test
    void testGetPage_PositivePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException  {
        int inputPage = 3;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, RequestService.class, requestService);
        assertEquals(inputPage - 1, result);
    }

    @Test
    public void testGetPage_NegativePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        int inputPage = -50;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, RequestService.class, requestService);
        assertEquals(0, result);
    }

    @Test
    void testGetCustomerRequest_ValidRequestIdAndEmail_ReturnsRequest(){
        Request request = TestUtil.createRequest(EMAIL);
        when(requestRepository.findById(request.getId())).thenReturn(Optional.of(request));
        when(requestRepository.existsByIdAndCustomerEmail(request.getId(), EMAIL)).thenReturn(true);

        Request foundRequest = requestService.getCustomerRequest(EMAIL, request.getId());

        assertEquals(request, foundRequest);
    }

    @Test
    void testGetCustomerRequest_OptionalEmptyRequestIdAndEmail_ThrowIllegalArgumentException(){
        Long id = 1L;
        when(requestRepository.findById(id)).thenReturn(Optional.empty());

        String expectedMessage = "По данному коду заявки нету";
        checkExceptionBehavior(id, expectedMessage);
    }

    @Test
    void testGetCustomerRequest_NotValidRequestIdAndEmail_ThrowIllegalArgumentException(){
        Request request = TestUtil.createRequest(EMAIL);
        when(requestRepository.findById(request.getId())).thenReturn(Optional.of(request));
        when(requestRepository.existsByIdAndCustomerEmail(request.getId(), EMAIL)).thenReturn(false);

        String expectedMessage = "Данная заявка не принадлежит вам";
        checkExceptionBehavior(request.getId(), expectedMessage);
    }

    private void checkExceptionBehavior(Long id, String expectedMessage){
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> requestService.getCustomerRequest(EMAIL, id));

        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }
}