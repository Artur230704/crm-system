package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class EmployeeServiceTest {

    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    EmployeeRepository employeeRepository;
    @Autowired
    private EmployeeService employeeService;

    private static final String EMAIL = "manager1@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    void setup() {
        Employee employee = createEmployee("manager1@mail.ru");
        when(employeeRepository.findByEmail(employee.getEmail())).thenReturn(Optional.of(employee));
        when(passwordEncoder.matches(eq("12345678"), anyString())).thenReturn(true);
    }

    @Test
    void testFindEmployee_ExistingEmployee() {
        String username = EMAIL;
        Employee optionalEmployee = createEmployee(username);
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(optionalEmployee));

        Employee employee = employeeService.findEmployee(username);

        assertAll(
                () -> assertNotNull(employee),
                () -> assertEquals("manager1@mail.ru", employee.getEmail())
        );
    }

    @Test
    void testFindEmployee_NonExistingEmployee() {
        String username = EMAIL;
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> employeeService.findEmployee(username));
    }

    @Test
    void testPasswordCheck_ValidPassword() {
        String username = EMAIL;
        String prevPassword = PASSWORD;

        Employee employee = createEmployee(username);

        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));
        when(passwordEncoder.matches(prevPassword, employee.getPassword()))
                .thenAnswer(invocationOnMock -> prevPassword.equals(employee.getPassword()));

        boolean result = employeeService.passwordCheck(username, prevPassword);

        assertTrue(result);
    }

    @Test
    void testChangePassword_PasswordChangeSuccessful() {
        String username = EMAIL;
        String prevPassword = PASSWORD;

        Employee initEmployee = createEmployee(username);

        ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
                .prevPassword(prevPassword)
                .newPassword(prevPassword)
                .newPasswordRepeat(prevPassword)
                .build();
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(initEmployee));
        employeeService.changePassword(username, changePasswordDTO);

        verify(employeeRepository, times(1)).save(any(Employee.class));
    }

    private Employee createEmployee(String username) {
        Employee employee = new Employee();
        employee.setEmail(username);
        employee.setId(1L);
        employee.setPassword("12345678");
        employee.setFullName("Manager1 manager1");
        employee.setNumber("+996712123456");
        employee.setInn("22222222222222");
        employee.setPost("Manager");
        employee.setLogin("manager1");
        employee.setEnabled(true);
        return employee;
    }

    @Test
    void testChangeProfileSettings_ProfileSettingsChangeSuccessful() {
        String username = EMAIL;
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail("email@gmail.com");

        Employee employee = createEmployee(username);
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));

        employeeService.changeProfileSettings(username, employeeDTO);

        verify(employeeRepository, times(1)).save(employee);
        assertEquals(employeeDTO.getEmail(), employee.getEmail());
    }

    @Test
    void testChangeProfileNumberSettings_NumberChangeSuccessful() {
        String username = EMAIL;
        Employee employee = createEmployee(username);
        EmployeeChangeNumberDTO employeeChangeNumberDTO = new EmployeeChangeNumberDTO();
        employeeChangeNumberDTO.setNumber("+996654654654");

        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));

        employeeService.changeProfileNumberSettings(username, employeeChangeNumberDTO);

        verify(employeeRepository, times(1)).save(employee);
        assertEquals(employeeChangeNumberDTO.getNumber(), employee.getNumber());
    }
}