package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Csrf;
import kg.attractor.esdp.mercsgroup.repositories.CsrfRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class CsrfServiceTest {

    @MockBean
    private CsrfRepository csrfRepository;

    @Autowired
    private CsrfService csrfService;

    @Test
    public void testGenerateCsrfLink(){
        String csrfToken = "csrfToken";
        Csrf csrf = csrfService.generateCsrfLink(csrfToken);
        verify(csrfRepository, times(1)).deleteAllByExpirationDateTimeBefore(any());
        verify(csrfRepository, times(1)).save(any());
        assertNull(csrf);
    }

    @Test
    public void testValidateCsrf_ExpirationTimeIsAfter(){
        String csrfToken = "csrfToken";
        when(csrfRepository.findByCsrfToken(csrfToken)).thenReturn(Optional.of(TestUtil.createCsrfObject(csrfToken)));
        boolean response = csrfService.validateCsrf(csrfToken);
        assertTrue(response);
        verify(csrfRepository, never()).delete(any());
    }

    @Test
    public void testValidateCsrf_ExpirationTimeIsBefore(){
        String csrfToken = "csrfToken";
        Csrf csrf = TestUtil.createCsrfObject(csrfToken);
        csrf.setExpirationDateTime(LocalDateTime.now().minusHours(1));
        when(csrfRepository.findByCsrfToken(csrfToken)).thenReturn(Optional.of(csrf));
        boolean response = csrfService.validateCsrf(csrfToken);
        assertFalse(response);
        verify(csrfRepository, times(1)).delete(csrf);
    }

    @Test
    public void testValidateCsrf_OptionalIsEmpty(){
        String csrfToken = "csrfToken";
        when(csrfRepository.findByCsrfToken(csrfToken)).thenReturn(Optional.empty());
        boolean response = csrfService.validateCsrf(csrfToken);
        assertFalse(response);
        verify(csrfRepository, never()).delete(any());
    }

    @Test
    public void testGetCsrfByToken(){
        String csrfToken = "csrfToken";
        Csrf csrf = TestUtil.createCsrfObject(csrfToken);
        when(csrfRepository.findByCsrfToken(csrfToken)).thenReturn(Optional.of(csrf));
        Optional<Csrf> optionalCsrf = csrfService.getCsrfByToken(csrfToken);
        assertTrue(optionalCsrf.isPresent());
        assertEquals(optionalCsrf.get(), csrf);
    }

    @Test
    public void testDeleteCsrf(){
        String csrfToken = "csrfToken";
        Csrf csrf = TestUtil.createCsrfObject(csrfToken);
        when(csrfRepository.findByCsrfToken(csrfToken)).thenReturn(Optional.of(csrf));
        csrfService.deleteCsrf(csrfToken);
        verify(csrfRepository, times(1)).delete(csrf);
    }
}