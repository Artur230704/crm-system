package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.PasswordRecoveryDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.entities.Token;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.TokenRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@SpringBootTest
class PasswordRecoveryServiceTest {

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private EmployeeRepository employeeRepository;

    @MockBean
    private TokenRepository tokenRepository;

    @Autowired
    private PasswordRecoveryService passwordRecoveryService;

    @Test
    public void testSendPasswordRecoveryTokenToEmail_CustomerFound(){
        String email = " ";
        when(customerRepository.findByEmail(email)).thenReturn(Optional.of(TestUtil.createCustomer(email)));
        boolean response = passwordRecoveryService.sendPasswordRecoveryTokenToEmail(email);
        assertTrue(response);
    }

    @Test
    public void testSendPasswordRecoveryTokenToEmail_EmployeeFound(){
        String email = " ";
        when(employeeRepository.findByEmail(email)).thenReturn(Optional.of(TestUtil.createEmployee()));
        boolean response = passwordRecoveryService.sendPasswordRecoveryTokenToEmail(email);
        assertTrue(response);
    }

    @Test
    public void testSendPasswordRecoveryTokenToEmail_NotFound(){
        boolean response = passwordRecoveryService.sendPasswordRecoveryTokenToEmail(any());
        assertFalse(response);
    }

    @Test
    public void testConfirmToken_CustomerFound(){
        String email = " ";
        String tokenValue = "1234";
        Customer customer = TestUtil.createCustomer(email);
        when(customerRepository.findByEmail(email)).thenReturn(Optional.of(customer));
        Token token = Token.builder()
                        .id(1L)
                        .token(tokenValue)
                        .user_id(customer.getId())
                        .role(TestUtil.createRole())
                        .build();
        when(tokenRepository.findByUserIdAndRoleId(customer.getId(), customer.getRole().getId()))
                .thenReturn(Optional.of(token));
        boolean response = passwordRecoveryService.confirmToken(email, tokenValue);
        assertTrue(response);

        verify(customerRepository, times(1)).findByEmail(email);
        verify(employeeRepository, times(1)).findByEmail(email);
        verify(tokenRepository, times(1)).findByUserIdAndRoleId(customer.getId(),
                customer.getRole().getId());
    }

    @Test
    public void testConfirmToken_EmployeeFound(){
        String email = " ";
        String tokenValue = "1234";
        Employee employee = TestUtil.createEmployee();
        when(employeeRepository.findByEmail(email)).thenReturn(Optional.of(employee));
        Token token = Token.builder()
                .id(1L)
                .token(tokenValue)
                .user_id(employee.getId())
                .role(TestUtil.createRole())
                .build();
        when(tokenRepository.findByUserIdAndRoleId(employee.getId(), employee.getRole().getId()))
                .thenReturn(Optional.of(token));
        boolean response = passwordRecoveryService.confirmToken(email, tokenValue);
        assertTrue(response);

        verify(customerRepository, times(1)).findByEmail(email);
        verify(employeeRepository, times(1)).findByEmail(email);
        verify(tokenRepository, times(1)).findByUserIdAndRoleId(employee.getId(),
                employee.getRole().getId());
    }

    @Test
    public void testConfirmToken_NotFound(){
        String email = " ";
        String tokenValue = " ";
        boolean response = passwordRecoveryService.confirmToken(email, tokenValue);
        assertFalse(response);

        verify(customerRepository, times(1)).findByEmail(email);
        verify(employeeRepository, times(1)).findByEmail(email);
        verify(tokenRepository, never()).findByUserIdAndRoleId(any(),
                any());
    }

    @Test
    public void testRecoverPassword_CustomerFound(){
        PasswordRecoveryDTO passwordRecoveryDTO = TestUtil.createPasswordRecoveryDto();
        passwordRecoveryDTO.setEmail(" ");
        String tokenValue = "1234";
        Customer customer = TestUtil.createCustomer(passwordRecoveryDTO.getEmail());
        Token token = Token.builder()
                .id(1L)
                .token(tokenValue)
                .user_id(customer.getId())
                .role(TestUtil.createRole())
                .build();
        when(customerRepository.findByEmail(passwordRecoveryDTO.getEmail())).thenReturn(Optional.of(customer));
        when(tokenRepository.findByUserIdAndRoleId(customer.getId(), customer.getRole().getId()))
                .thenReturn(Optional.of(token));

        passwordRecoveryService.recoverPassword(passwordRecoveryDTO);

        verify(customerRepository, times(1)).findByEmail(passwordRecoveryDTO.getEmail());
        verify(employeeRepository, times(1)).findByEmail(passwordRecoveryDTO.getEmail());
        verify(tokenRepository, times(1)).findByUserIdAndRoleId(customer.getId(),
                customer.getRole().getId());
        verify(tokenRepository, times(1)).delete(token);
        verify(customerRepository, times(1)).setPasswordById(any(), any());
    }

    @Test
    public void testRecoverPassword_EmployeeFound(){
        PasswordRecoveryDTO passwordRecoveryDTO = TestUtil.createPasswordRecoveryDto();
        passwordRecoveryDTO.setEmail(" ");
        String tokenValue = "1234";
        Employee employee = TestUtil.createEmployee();
        Token token = Token.builder()
                .id(1L)
                .token(tokenValue)
                .user_id(employee.getId())
                .role(TestUtil.createRole())
                .build();
        when(employeeRepository.findByEmail(passwordRecoveryDTO.getEmail())).thenReturn(Optional.of(employee));
        when(tokenRepository.findByUserIdAndRoleId(employee.getId(), employee.getRole().getId()))
                .thenReturn(Optional.of(token));

        passwordRecoveryService.recoverPassword(passwordRecoveryDTO);

        verify(customerRepository, times(1)).findByEmail(passwordRecoveryDTO.getEmail());
        verify(employeeRepository, times(1)).findByEmail(passwordRecoveryDTO.getEmail());
        verify(tokenRepository, times(1)).findByUserIdAndRoleId(employee.getId(),
                employee.getRole().getId());
        verify(tokenRepository, times(1)).delete(token);
        verify(employeeRepository, times(1)).setPasswordById(any(), any());
    }

    @Test
    public void getDeclaredTokenSendingProcessMethod() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException{
        Customer customer = TestUtil.createCustomer(" ");
        String tokenValue = "1234";
        Token token = Token.builder()
                .id(1L)
                .token(tokenValue)
                .user_id(customer.getId())
                .role(TestUtil.createRole())
                .build();
        when(tokenRepository.findByUserIdAndRoleId(customer.getId(), customer.getRole().getId()))
                .thenReturn(Optional.of(token));
        Method getPageMethod = PasswordRecoveryService.class.getDeclaredMethod("sendingProcess", Long.class,
                Role.class, String.class);
        getPageMethod.setAccessible(true);

        getPageMethod.invoke(passwordRecoveryService, customer.getId(), customer.getRole(), customer.getEmail());
        getPageMethod.setAccessible(false);
        verify(tokenRepository, times(1)).setTokenByUserIdAndRole(any(), any(), any());
        verify(tokenRepository, never()).save(any());
    }

    @Test
    public void getDeclaredTokenSendingProcessMethod_TokenCreation() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException{
        Customer customer = TestUtil.createCustomer(" ");
        Method getPageMethod = PasswordRecoveryService.class.getDeclaredMethod("sendingProcess", Long.class,
                Role.class, String.class);
        getPageMethod.setAccessible(true);

        getPageMethod.invoke(passwordRecoveryService, customer.getId(), customer.getRole(), customer.getEmail());
        getPageMethod.setAccessible(false);
        verify(tokenRepository, never()).setTokenByUserIdAndRole(any(), any(), any());
        verify(tokenRepository, times(1)).save(any());
    }

    @Test
    public void getDeclaredHashPasswordMethod() throws NoSuchMethodException,
          InvocationTargetException, IllegalAccessException{
        Method getPageMethod = PasswordRecoveryService.class.getDeclaredMethod("hashPassword", String.class);
        getPageMethod.setAccessible(true);
        String password = "12345678";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String response = (String) getPageMethod.invoke(passwordRecoveryService, password);
        getPageMethod.setAccessible(false);
        assertTrue(passwordEncoder.matches(password, response));

        assertFalse(passwordEncoder.matches("wrongPassword", response));
    }

    @Test
    public void getDeclaredGenerateRandomNumberStringMethod() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException{
        Method getPageMethod = PasswordRecoveryService.class.getDeclaredMethod("generateRandomNumberString");
        getPageMethod.setAccessible(true);

        String response = (String) getPageMethod.invoke(passwordRecoveryService);
        getPageMethod.setAccessible(false);
        assertEquals(PasswordRecoveryService.PASSWORD_RECOVERY_TOKEN_LENGTH, response.length());
    }
}