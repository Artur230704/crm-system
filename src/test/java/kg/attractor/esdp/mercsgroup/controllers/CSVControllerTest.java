package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.services.CSVService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CSVControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CSVService csvService;

    private static final String EMAIL = "manager1@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testUploadCSVFile() throws Exception{
        MockMultipartFile file = TestUtil.createMockMultipartFile("test.csv", "text/csv");

        when(csvService.uploadCSVFile(file)).thenReturn(true);

        mockMvc.perform(multipart("/csv")
                .file(file))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testNotValidCSVFile() throws Exception{
        MockMultipartFile file = TestUtil.createMockMultipartFile("test.pdf", "application/pdf");

        when(csvService.uploadCSVFile(file)).thenReturn(false);

        mockMvc.perform(multipart("/csv")
                .file(file))
                .andExpect(status().is4xxClientError());
    }
}