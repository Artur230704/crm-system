package kg.attractor.esdp.mercsgroup.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CsrfControllerTest {

    @Autowired
    private CsrfController csrfController;

    @Test
    public void testGetCsrfToken() {
        CsrfToken csrfToken = new DefaultCsrfToken("X-CSRF-TOKEN", "_csrf", "csrfTokenValue");
        ResponseEntity<String> response = csrfController.getCsrfToken(csrfToken);
        int expectedStatusCode = 200;
        assertEquals(expectedStatusCode, response.getStatusCodeValue());
    }
}