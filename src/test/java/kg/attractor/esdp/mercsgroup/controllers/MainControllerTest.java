package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class MainControllerTest {

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MainController mainController;

    @Test
    public void testGetAdminProfilePage(){
        Model model = new ConcurrentModel();
        String email = "admin@mail.ru";
        Authentication authentication = new UsernamePasswordAuthenticationToken(email,
                "12345678", List.of(new SimpleGrantedAuthority("Admin")));
        String expectedResponse = "crm-main-admin";
        when(employeeService.findEmployee(email)).thenReturn(TestUtil.createEmployee());
        String response = mainController.getProfile(model, authentication);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetCustomerProfilePage(){
        Model model = new ConcurrentModel();
        String email = "cr1@mail.ru";
        Authentication authentication = new UsernamePasswordAuthenticationToken(email,
                "12345678", List.of(new SimpleGrantedAuthority("Customer")));
        String expectedResponse = "crm-main-customer";
        when(customerService.findCustomer(email)).thenReturn(TestUtil.createCustomer(email));
        String response = mainController.getProfile(model, authentication);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetManagerProfilePage(){
        Model model = new ConcurrentModel();
        String email = "manager1@mail.ru";
        Authentication authentication = new UsernamePasswordAuthenticationToken(email,
                "12345678", List.of(new SimpleGrantedAuthority("Manager")));
        String expectedResponse = "crm-main-employee";
        when(employeeService.findEmployee(email)).thenReturn(TestUtil.createEmployee());
        String response = mainController.getProfile(model, authentication);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetLoginPage(){
        Model model = new ConcurrentModel();
        Authentication authentication = new UsernamePasswordAuthenticationToken(null,
                null, Collections.emptyList());
        String expectedResponse = "redirect:/login";
        String response = mainController.getProfile(model, authentication);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetErrorPage(){
        String expectedResponse = "error/404";
        String response = mainController.handle404Error();
        assertEquals(expectedResponse, response);
    }
}