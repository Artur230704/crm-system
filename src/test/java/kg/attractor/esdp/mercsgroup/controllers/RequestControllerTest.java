package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.request.NewRequestDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Delivery;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.DeliveryService;
import kg.attractor.esdp.mercsgroup.services.RequestService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RequestControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private DeliveryService deliveryService;

    @Autowired
    private RequestController requestController;

    private static final String EMAIL = "cr1@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void testGetNewRequest_ValidAuthentication_NoErrorOrMessage(){
        List<GrantedAuthority> authorities = TestUtil.createGrantedAuthorityList("Customer");
        UserDetails userDetails = new User(EMAIL, PASSWORD, authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, PASSWORD, authorities);
        Model model = new ConcurrentModel();
        Customer customer = TestUtil.createCustomer(EMAIL);
        List<Delivery> deliveries = TestUtil.createDeliveryList();
        when(customerService.findCustomer(EMAIL)).thenReturn(customer);
        when(deliveryService.getAll()).thenReturn(deliveries);

        String response = requestController.getNewRequest(model, authentication, null, null);

        verify(customerService, times(1)).findCustomer(EMAIL);
        verify(deliveryService, times(1)).getAll();

        String expectedPageName = "add-new-request";
        assertEquals(expectedPageName, response);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testAddNewRequest_Successful() throws Exception {
        NewRequestDTO newRequestDto = TestUtil.createNewRequestDto();

        mockMvc.perform(post("/requests/new")
                .param("customerId", newRequestDto.getCustomerId().toString())
                .param("product", newRequestDto.getProduct())
                .param("description", newRequestDto.getDescription())
                .param("information", newRequestDto.getInformation())
                .param("quantity", newRequestDto.getQuantity())
                .param("deliveryId", newRequestDto.getDeliveryId().toString())
                .with(csrf()))
                .andExpect(status().isOk());

        verify(requestService, times(1)).addNewRequest(newRequestDto);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testAddNewRequest_Unsuccessful() throws Exception {
        mockMvc.perform(post("/requests/new")
                .with(csrf()))
                .andExpect(status().is4xxClientError());

        verify(requestService, never()).addNewRequest(any());
    }
}