package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.GuestRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.ManagerRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.services.AdminService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTest {

    private static final String EMAIL = "admin@mail.ru";
    private static final String PASSWORD = "12345678";
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdminService adminService;

    @MockBean
    private BindingResult bindingResult;

    @Autowired
    private AdminController adminController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileNumberChanges_ProfileNumberChangeSuccessful() throws Exception {
        String username = EMAIL;
        EmployeeChangeNumberDTO employeeDTO = new EmployeeChangeNumberDTO();
        employeeDTO.setNumber("+996777888998");

        Employee employee = createEmployee(username);
        when(adminService.findEmployee(username)).thenReturn(employee);
        setupForNumberChanges(employeeDTO, "/");

        verify(adminService, times(1)).changeProfileNumberSettings(username, employeeDTO);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileNumberChanges_ProfileNumberChangeUnsuccessful() throws Exception {
        EmployeeChangeNumberDTO employeeDTO = new EmployeeChangeNumberDTO();
        employeeDTO.setNumber(" ");
        setupForNumberChanges(employeeDTO, "/?error");

        verify(adminService, never()).changeProfileNumberSettings(EMAIL, employeeDTO);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileChange_ProfileChangeSuccessful() throws Exception {
        String username = EMAIL;
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail("newemail@example.com");

        Employee employee = createEmployee(username);
        when(adminService.findEmployee(username)).thenReturn(employee);
        setupForEmailChanges(employeeDTO, "/");

        verify(adminService, times(1)).changeProfileSettings(username, employeeDTO);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileChange_ProfileChangeUnsuccessful() throws Exception {
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail(" ");

        setupForEmailChanges(employeeDTO, "/?error");
        verify(adminService, never()).changeProfileSettings(EMAIL, employeeDTO);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testChangePassword_PasswordChangeSuccessful() throws Exception {
        String prevPassword = PASSWORD;
        setupForChangePassword(prevPassword);
        verify(adminService, times(1)).passwordCheck(EMAIL, prevPassword);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testChangePassword_PasswordChangeUnsuccessful() throws Exception {
        String prevPassword = " ";
        setupForChangePassword(prevPassword);
        verify(adminService, never()).passwordCheck(EMAIL, prevPassword);
    }

    @Test
    public void testGetCustomersReplacePageWithDefaultParameters(){
        String role = "Customer";
        Integer defaultPage = 1;
        Integer defaultSize = 5;
        Page<Customer> page = new PageImpl<>(Collections.singletonList(TestUtil.createCustomer(EMAIL)));
        when(adminService.getCustomers(defaultPage, defaultSize, role)).thenReturn(page);

        Model model = new ConcurrentModel();
        String response = adminController.getCustomers(model, defaultPage, defaultSize);

        String expectedPageName = "/admin/block-replacement/customers-replace";
        verify(adminService, times(1)).getCustomers(defaultPage, defaultSize, role);
        assertEquals(expectedPageName, response);
    }

    @Test
    public void testGetGuestsReplacePageWithDefaultParameters(){
        String role = "Guest";
        Integer defaultPage = 1;
        Integer defaultSize = 5;
        Page<Customer> page = new PageImpl<>(Collections.singletonList(TestUtil.createCustomer(EMAIL)));
        when(adminService.getCustomers(defaultPage, defaultSize, role)).thenReturn(page);

        Model model = new ConcurrentModel();
        String response = adminController.getGuests(model, defaultPage, defaultSize);

        String expectedPageName = "/admin/block-replacement/guests-replace";
        verify(adminService, times(1)).getCustomers(defaultPage, defaultSize, role);
        assertEquals(expectedPageName, response);
    }

    @Test
    public void testGetEmployeesReplacePageWithDefaultParameters(){
        String role = "Manager";
        Integer defaultPage = 1;
        Integer defaultSize = 5;
        Page<Employee> page = new PageImpl<>(Collections.singletonList(TestUtil.createEmployee()));
        when(adminService.getManagers(defaultPage, defaultSize, role)).thenReturn(page);

        Model model = new ConcurrentModel();
        String response = adminController.getEmployees(model, defaultPage, defaultSize);

        String expectedPageName = "/admin/block-replacement/employees-replace";
        verify(adminService, times(1)).getManagers(defaultPage, defaultSize, role);
        assertEquals(expectedPageName, response);
    }

    @Test
    public void testManagerRegistrationPage(){
        String response = adminController.managerRegistration();
        assertEquals("/admin/manager-registration", response);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testRegisterManager_RegisteredSuccessfully() throws Exception{
        ManagerRegistrationDTO managerRegistrationDTO = TestUtil.createManagerRegistrationDto();
        when(adminService.registerNewManager(any(), any())).thenReturn(true);
        mockMvc.perform(post("/admin/employee-registration")
                .param("email", managerRegistrationDTO.getEmail())
                .param("inn", managerRegistrationDTO.getInn())
                .param("login", managerRegistrationDTO.getLogin())
                .param("phone", managerRegistrationDTO.getPhone())
                .param("fullName", managerRegistrationDTO.getFullName())
                .param("password", managerRegistrationDTO.getPassword())
                .with(csrf()))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testRegisterManager_RegisteredUnsuccessfully() {
        ManagerRegistrationDTO managerRegistrationDTO = TestUtil.createManagerRegistrationDto();
        managerRegistrationDTO.setEmail(" ");
        String expectedResponse = "/admin/manager-registration";
        Model model = new ConcurrentModel();
        String response = adminController.employeeRegistration(managerRegistrationDTO, bindingResult, model);
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetGuestRegistrationPage(){
        String expectedResponse = "/admin/guest-registration";
        String response = adminController.guestsRegistration();
        assertEquals(expectedResponse, response);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testPostGuestRegistrationMethod_SuccessfullyRegistration() throws Exception{
        GuestRegistrationDTO guestRegistrationDTO = TestUtil.createGuestRegistrationDto();
        when(adminService.registerNewGuest(any(), any())).thenReturn(true);
        mockMvc.perform(post("/admin/guest-registration")
                .param("email", guestRegistrationDTO.getEmail())
                .param("name", guestRegistrationDTO.getName())
                .param("phone", guestRegistrationDTO.getPhone())
                .param("company", guestRegistrationDTO.getCompany())
                .with(csrf()))
                .andExpect(status().is3xxRedirection());
        verify(adminService, times(1)).registerNewGuest(any(), any());
    }

    @Test
    public void testPostGuestRegistrationMethod_UnsuccessfullyRegistration() {
        GuestRegistrationDTO guestRegistrationDTO = GuestRegistrationDTO.builder()
                .email(" ")
                .build();
        Model model = new ConcurrentModel();
        String expectedResponse = "/admin/guest-registration";
        String response = adminController.guestsRegistration(guestRegistrationDTO, bindingResult, model);
        assertEquals(expectedResponse, response);
    }


    private void setupForNumberChanges(EmployeeChangeNumberDTO employeeDTO, String url) throws Exception{
        mockMvc.perform(post("/admin/profile/changes/number")
                        .param("number", employeeDTO.getNumber())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(url));
    }

    private void setupForEmailChanges(EmployeeChangeDTO employeeDTO, String url) throws Exception{
        mockMvc.perform(post("/admin/profile/changes/email")
                        .param("email", employeeDTO.getEmail())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(url));
    }

    private void setupForChangePassword(String prevPassword) throws Exception {
        String username = EMAIL;
        ChangePasswordDTO changePasswordDTO = createChangePasswordDTO(prevPassword);

        Employee employee = createEmployee(username);

        when(adminService.findEmployee(username)).thenReturn(employee);
        doNothing().when(adminService).changePassword(username, changePasswordDTO);

        mockMvc.perform(post("/admin/profile/changePassword")
                        .param("prevPassword", prevPassword)
                        .param("newPassword", prevPassword)
                        .param("newPasswordRepeat", prevPassword)
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/?error"));
    }

    private ChangePasswordDTO createChangePasswordDTO(String prevPassword){
        return ChangePasswordDTO.builder()
                .newPassword(prevPassword)
                .newPasswordRepeat(prevPassword)
                .prevPassword(prevPassword)
                .build();
    }

    private Employee createEmployee(String username){
        Employee employee = new Employee();
        employee.setEmail(username);
        employee.setFullName("Admin admin");
        employee.setNumber("+996777888998");
        employee.setInn("44444444444444");
        employee.setPost("Administrator");
        employee.setLogin("admin");
        employee.setPassword("12345678");
        return employee;
    }
}