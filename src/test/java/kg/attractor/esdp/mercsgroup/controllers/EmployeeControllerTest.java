package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import kg.attractor.esdp.mercsgroup.services.OrderService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private OrderService orderService;

    private static final String EMAIL = "manager1@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileNumberChanges_ProfileNumberChangeSuccessful() throws Exception {
        EmployeeChangeNumberDTO employeeDTO = new EmployeeChangeNumberDTO();
        employeeDTO.setNumber("+996777888999");

        mockMvc.perform(post("/employees/profile/changes/number")
                        .param("number", employeeDTO.getNumber())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));

        verify(employeeService, times(1)).changeProfileNumberSettings(EMAIL, employeeDTO);
    }


    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileNumberChanges_ProfileNumberChangeUnsuccessful() throws Exception {
        EmployeeChangeNumberDTO employeeDTO = new EmployeeChangeNumberDTO();
        employeeDTO.setNumber(" ");

        mockMvc.perform(post("/employees/profile/changes/number")
                        .param("number", employeeDTO.getNumber())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/?error"));

        verify(employeeService, never()).changeProfileNumberSettings(EMAIL, employeeDTO);
    }


    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileChange_ProfileChangeSuccessful() throws Exception {
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail("newemail@example.com");

        mockMvc.perform(post("/employees/profile/changes/email")
                        .param("email", employeeDTO.getEmail())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/"));

        verify(employeeService, times(1)).changeProfileSettings(EMAIL, employeeDTO);
    }


    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testApplyCustomerProfileChange_ProfileChangeUnsuccessful() throws Exception {
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail(" ");

        mockMvc.perform(post("/employees/profile/changes/email")
                        .param("email", employeeDTO.getEmail())
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/?error"));

        verify(employeeService, never()).changeProfileSettings(EMAIL, employeeDTO);
    }


    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testChangePassword_PasswordChangeSuccessful() throws Exception {
        String username = EMAIL;
        String prevPassword = PASSWORD;

        ChangePasswordDTO changePasswordDTO = createChangePasswordDTO(prevPassword);

        Employee employee = createEmployee(username);

        when(employeeService.findEmployee(username)).thenReturn(employee);
        doNothing().when(employeeService).changePassword(username, changePasswordDTO);

        mockMvc.perform(post("/employees/profile/changePassword")
                        .param("prevPassword", prevPassword)
                        .param("newPassword", prevPassword)
                        .param("newPasswordRepeat", prevPassword)
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/?error"));

        verify(employeeService, times(1)).passwordCheck(username, prevPassword);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testChangePassword_PasswordChangeUnsuccessful() throws Exception {
        String username = EMAIL;
        String prevPassword = " ";

        ChangePasswordDTO changePasswordDTO = createChangePasswordDTO(prevPassword);

        Employee employee = createEmployee(username);

        when(employeeService.findEmployee(username)).thenReturn(employee);
        doNothing().when(employeeService).changePassword(username, changePasswordDTO);

        mockMvc.perform(post("/employees/profile/changePassword")
                        .param("prevPassword", prevPassword)
                        .param("newPassword", prevPassword)
                        .param("newPasswordRepeat", prevPassword)
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/?error"));

        verify(employeeService, never()).passwordCheck(username, prevPassword);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    public void testGetOrdersByEmployee_ReturnsEmployeeOrdersPage() throws Exception{
        String sortBy = "customer";
        int page = 0;
        int size = 5;
        int sortOrder = 1;
        Pageable pageable = PageRequest.of(page, size);
        String status = TestUtil.createStatus().getStatus();
        String customerName = "TestCustomer";
        String managerName = "TestManager";

        Page<Order> ordersPage = new PageImpl<>(Collections.singletonList(TestUtil.createOrder(EMAIL)), pageable, 0);

        when(orderService.searchOrdersAndSort(page, size,  Optional.of(customerName),
                Optional.of(managerName), Optional.of(status),
                sortBy, sortOrder, EMAIL)).thenReturn(ordersPage);
        when(orderService.findOrdersWithExpiredLeads(EMAIL)).thenReturn(Collections.singletonList(1L));

        mockMvc.perform(get("/employees/orders-replace")
                .param("customer", customerName)
                .param("manager", managerName)
                .param("status", status)
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("page"))
                .andExpect(view().name("block-replacement/employee-orders-replace"));
    }

    private ChangePasswordDTO createChangePasswordDTO(String prevPassword) {
        return ChangePasswordDTO.builder()
                .newPassword(prevPassword)
                .newPasswordRepeat(prevPassword)
                .prevPassword(prevPassword)
                .build();
    }

    private Employee createEmployee(String username) {
        Employee employee = new Employee();
        employee.setEmail(username);
        employee.setFullName("Manager1 manager1");
        employee.setNumber("+996712123456");
        employee.setInn("22222222222222");
        employee.setPost("Manager");
        employee.setLogin("manager1");
        employee.setPassword("12345678");
        return employee;
    }
}
