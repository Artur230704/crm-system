package kg.attractor.esdp.mercsgroup.validators;

import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueINNValidator implements ConstraintValidator<UniqueINN, String> {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void initialize(UniqueINN constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !employeeService.isINNExists(s);
    }
}
