package kg.attractor.esdp.mercsgroup.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class TelegramUtil {
    private static final String TELEGRAM_BOT_TOKEN = "6403876503:AAG0YVm1C9YsB2c-Xhz7j_yHYZVi49UdEAI";
    private static final String CHAT_ID = "-819871405";

    public static void sendNotification (String message) {
        String apiEndpoint = "https://api.telegram.org/bot" + TELEGRAM_BOT_TOKEN + "/sendMessage";

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(apiEndpoint);

        try {
            String jsonPayload = String.format("{\"chat_id\":\"%s\",\"text\":\"%s\"}", CHAT_ID, message);
            StringEntity entity = new StringEntity(jsonPayload, StandardCharsets.UTF_8);

            httpPost.setEntity(entity);
            httpPost.setHeader("Content-Type", "application/json; charset=UTF-8");

            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                String responseBody = EntityUtils.toString(responseEntity);
                System.out.println(responseBody);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
