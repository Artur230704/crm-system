package kg.attractor.esdp.mercsgroup.utils;

import lombok.extern.log4j.Log4j2;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Log4j2
public class EmailUtil {
    private static final String SMTP_HOST = "smtp.gmail.com";
    private static final int SMTP_PORT = 587;
    private static final String EMAIL = "mercs.passbot@gmail.com";
    private static final String PASSWORD = "xfkfbbggpbicvgkr";

    public static void sendPasswordResetEmail(String recipientEmail, String token) {
        Session session = getSession();

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
            message.setSubject("Восстановление пароля");
            message.setText("Введите токен на странице восстановления: " + token);

            Transport.send(message);
            log.info("Токен сброса был успешно отправлен");
        } catch (MessagingException e) {
            log.warn("Произошла ошибка при отправлении токена на почту. Ошибка: " + e.getMessage());
        }
    }

    private static Session getSession() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", SMTP_HOST);
        props.put("mail.smtp.port", SMTP_PORT);

        return Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(EMAIL, PASSWORD);
            }
        });
    }


    public static void sendOfferToClient(String recipientEmail, String filePath) {
        Session session = getSession();

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
            message.setSubject("Коммерческое предложение");

            Multipart multipart = new MimeMultipart();

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText("Здравствуйте! " +
                    "Отправляем Вам наше коммерческое " +
                    "предложение на оставленную вами заявку.");
            multipart.addBodyPart(messageBodyPart);

            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filePath);
            attachmentBodyPart.setDataHandler(new DataHandler(source));
            attachmentBodyPart.setFileName(source.getName());

            multipart.addBodyPart(attachmentBodyPart);
            message.setContent(multipart);
            Transport.send(message);

            System.out.println("Успешно отправлено");
        } catch (MessagingException e) {
            System.out.println("Не отправлено");
        }
    }
}
