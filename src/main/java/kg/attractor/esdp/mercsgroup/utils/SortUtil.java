package kg.attractor.esdp.mercsgroup.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class SortUtil {
    public static Pageable sort(Integer page, Integer size, String[] sort){
        page = getPage(page);
        Sort.Order[] orders = new Sort.Order[sort.length];

        for (int i = 0; i < sort.length; i++) {
            orders[i] = new Sort.Order(Sort.Direction.ASC, sort[i]);
        }
        Sort sorting = Sort.by(orders);
        return PageRequest.of(page, size, sorting);
    }

    private static Integer getPage(Integer page) {
        if (page != null && page > 0) {
            return page - 1;
        }
        return 0;
    }
}


