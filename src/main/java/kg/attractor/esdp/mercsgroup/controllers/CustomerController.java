package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.RequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.DeliveryDTO;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    private final RequestService requestService;
    private final OrderService orderService;
    private final DeliveryService deliveryService;

    @GetMapping("/customers/profile/requests/{id}")
    public String getCustomerRequest(@AuthenticationPrincipal UserDetails userDetails, Model model, @PathVariable Long id) {
        String email = userDetails.getUsername();
        var request = RequestDTO.from(requestService.getCustomerRequest(email, id));
        model.addAttribute("request", request);
        return "request";
    }

    @GetMapping("/customers/requests-replace")
    public String replaceRequests(Model model,
                                  @AuthenticationPrincipal UserDetails userDetails,
                                  @RequestParam(defaultValue = "1", required = false) Integer page,
                                  @RequestParam(defaultValue = "5", required = false) Integer size
    ) {
        String email = userDetails.getUsername();
        var customerDTO = CustomerDTO.from(customerService.findCustomer(email));
        var requestsPageable = requestService.getCustomerRequests(customerDTO.getId(), page, size);
        var requests = requestsPageable.stream()
                .map(RequestDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("currentPage", requestsPageable.getNumber() + 1);
        model.addAttribute("totalPages", requestsPageable.getTotalPages());
        model.addAttribute("requests", requests);
        return "block-replacement/customer-request-replace";
    }

    @GetMapping("/customers/payments-replace")
    public String replacePayments(Model model,
                                  @AuthenticationPrincipal UserDetails userDetails,
                                  @RequestParam(defaultValue = "1", required = false) Integer page,
                                  @RequestParam(defaultValue = "5", required = false) Integer size){
        String email = userDetails.getUsername();
        var payments = orderService.getCustomerPayments(email, page, size);
        model.addAttribute("page", payments);
        return "block-replacement/customer-payments-details-replace";
    }

    @GetMapping("/customers/orders-replace")
    public String replaceOrders(
            Model model,
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(defaultValue = "1", required = false) Integer page,
            @RequestParam(defaultValue = "5", required = false) Integer size,
            @RequestParam(required = false, defaultValue = "request") String sortBy,
            @RequestParam(defaultValue = "1") Integer sortOrder,
            @RequestParam(name = "productName") Optional<String> productName,
            @RequestParam(name = "description") Optional<String> description,
            @RequestParam(name = "status", required = false) Optional<String> status
    ) {
        String email = userDetails.getUsername();
        var customerDTO = CustomerDTO.from(customerService.findCustomer(email));
        var ordersPageable = orderService.getCustomerOrders(customerDTO.getId(), page, size, sortBy, sortOrder,productName,description,status);
        model.addAttribute("sortOrder", sortOrder);
        model.addAttribute("sortBy", sortBy);
        return fillOrdersModel(model, ordersPageable);
    }

    @GetMapping("/customers/orders-replace/search-sort")
    public String getCustomerOrderedAndSorted(@RequestParam(defaultValue = "1", required = false) Integer page,
                                              @RequestParam(defaultValue = "5", required = false) Integer size,
                                              @RequestParam(name = "productName") Optional<String> productName,
                                              @RequestParam(name = "description") Optional<String> description,
                                              @RequestParam(name = "status", required = false) Optional<String> status,
                                              @AuthenticationPrincipal UserDetails userDetails, Model model) {
        String email = userDetails.getUsername();
        var customerDTO = CustomerDTO.from(customerService.findCustomer(email));
        model.addAttribute("search","search");
        productName.ifPresent(s -> model.addAttribute("productName", s));
        description.ifPresent(s -> model.addAttribute("description",s));
        status.ifPresent(s -> model.addAttribute("status",s));
        var ordersPageable = orderService.searchCustomerOrdersAndSort(customerDTO.getId(), page, size, productName,
                description, status);
        return fillOrdersModel(model, ordersPageable);
    }



    private String fillOrdersModel(Model model, Page<Order> ordersPageable) {
        var orders = ordersPageable.stream()
                .map(OrderDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("currentPage", ordersPageable.getNumber() + 1);
        model.addAttribute("totalPages", ordersPageable.getTotalPages());
        model.addAttribute("orders", orders);
        return "block-replacement/customer-orders-replace";
    }

    @GetMapping("/customers/new-request-replace")
    public String replaceNewRequest(
            Model model,
            Authentication authentication,
            @RequestParam(required = false) String message,
            @RequestParam(required = false) String error
    ){
        var customer = CustomerDTO.from(customerService.findCustomer(authentication.getName()));
        model.addAttribute("customer", customer);
        model.addAttribute("message", message != null);
        model.addAttribute("error", error != null);
        var deliveries = deliveryService.getAll().stream()
                .map(DeliveryDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("deliveries", deliveries);
        return "/block-replacement/customer-new-request-replace";
    }

    @PostMapping("/customers/profile/changes/email")
    public String applyCustomerProfileChange(@AuthenticationPrincipal UserDetails userDetails,
                                             @Valid CustomerChangeDTO customerChangeDTO, BindingResult bindingResult,
                                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String userName = userDetails.getUsername();
        customerService.changeProfileSettings(userName, customerChangeDTO);
        return "redirect:/";
    }

    @PostMapping("/customers/profile/changes/number")
    public String applyCustomerProfileNumberChanges(@Valid CustomerChangeNumberDTO customerChangeNumberDTO,
                                                    BindingResult bindingResult,
                                                    @AuthenticationPrincipal UserDetails userDetails,
                                                    RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String username = userDetails.getUsername();
        customerService.changeProfileNumberSettings(username, customerChangeNumberDTO);
        return "redirect:/";
    }

    @PostMapping("/customers/profile/changePassword")
    public String changePassword(@AuthenticationPrincipal UserDetails userDetails,
                                 @Valid ChangePasswordDTO changePasswordDTO,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        String username = userDetails.getUsername();

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        if (changePasswordDTO.getNewPassword().equals(changePasswordDTO.getNewPasswordRepeat()) &&
                customerService.passwordCheck(username, changePasswordDTO.getPrevPassword())) {
            customerService.changePassword(username, changePasswordDTO);
            redirectAttributes.addFlashAttribute("passwordChanged", true);
        } else {
            redirectAttributes.addFlashAttribute("passwordError", true);
        }

        return "redirect:/?error";
    }


}
