package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeDTO;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final CustomerService customerService;
    private final EmployeeService employeeService;

    @GetMapping("/")
    public String getProfile(Model model, Authentication authentication) {
        if (authentication!=null) {
            String email = authentication.getName();

            if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Admin"))) {
                var admin = EmployeeDTO.from(employeeService.findEmployee(email));
                model.addAttribute("employee", admin);
                return "crm-main-admin";
            } else if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Customer"))) {
                var customerDTO = CustomerDTO.from(customerService.findCustomer(email));
                model.addAttribute("customer", customerDTO);
                return "crm-main-customer";
            } else if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("Manager"))) {
                var employee = employeeService.findEmployee(email);
                model.addAttribute("employee", employee);
                return "crm-main-employee";
            }
        }

        return "redirect:/login";
    }

    @RequestMapping("/error/404")
    public String handle404Error() {
        return "error/404";
    }
}
