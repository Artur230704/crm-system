package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeFullNameDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeLoginDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderAndPaymentDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.ManagerRequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.RequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.DeliveryDTO;
import kg.attractor.esdp.mercsgroup.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeService employeeService;
    private final OrderService orderService;
    private final RequestService requestService;
    private final CustomerService customerService;
    private final DeliveryService deliveryService;


    @GetMapping("/employees/orders-replace")
    public String getOrdersByEmployee(Model model,
                                      @AuthenticationPrincipal UserDetails userDetails,
                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestParam(required = false, defaultValue = "5") int size,
                                      @RequestParam(required = false, defaultValue = "customer") String sortBy,
                                      @RequestParam(defaultValue = "1") int sortOrder,
                                      @RequestParam(name = "customer", required = false) Optional<String> customerName,
                                      @RequestParam(name = "manager", required = false) Optional<String> managerName,
                                      @RequestParam(name = "status", required = false) Optional<String> status) {
        String email = userDetails.getUsername();
        var ordersPageable = orderService.searchOrdersAndSort(page, size, customerName, managerName, status, sortBy, sortOrder, email)
                .map(OrderDTO::from);
        var expiredLeads = orderService.findOrdersWithExpiredLeads(email);

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("sortOrder", sortOrder);
        model.addAttribute("expiredLeads", expiredLeads);
        model.addAttribute("page", ordersPageable);
        model.addAttribute("customerName", customerName);
        model.addAttribute("managerName", managerName);
        model.addAttribute("status", status);
        return "block-replacement/employee-orders-replace";
    }

    @GetMapping("/employees/orders-replace/search-sort")
    public String replaceSearchOrders(Model model,
                                      @RequestParam(defaultValue = "1", required = false) Integer page,
                                      @RequestParam(defaultValue = "5", required = false) Integer size,
                                      @RequestParam(name = "customer") Optional<String> customerName,
                                      @RequestParam(name = "manager") Optional<String> managerName,
                                      @RequestParam(name = "status") Optional<String> status,
                                      @AuthenticationPrincipal UserDetails userDetails) {
        String email = userDetails.getUsername();
        var ordersPageable = orderService.searchOrdersAndSort(page, size, customerName,
                        managerName, status, email)
                .map(OrderDTO::from);
        model.addAttribute("search", "search");
        customerName.ifPresent(s -> model.addAttribute("customerName", s));
        managerName.ifPresent(s -> model.addAttribute("managerName", s));
        status.ifPresent(s -> model.addAttribute("status", s));
        model.addAttribute("page", ordersPageable);
        return "block-replacement/employee-orders-replace";
    }


    @GetMapping("/employees/requests-replace")
    public String getAllRequests(Model model,
                                 @RequestParam(value = "page", defaultValue = "0") int page,
                                 @RequestParam(required = false, defaultValue = "5") int size) {
        var requestsPageable = requestService.findAllByStatus("Новый", page, size);
        var requests = requestsPageable.stream()
                .map(RequestDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("page", requestsPageable);
        model.addAttribute("currentPage", requestsPageable.getNumber() + 1);
        model.addAttribute("totalPages", requestsPageable.getTotalPages());
        model.addAttribute("requests", requests);
        return "block-replacement/employee-request-replace";
    }

    @GetMapping("/employees/customers-replace")
    public String getAllCustomers(Model model,
                                  @RequestParam(value = "page", defaultValue = "0") int page,
                                  @RequestParam(required = false, defaultValue = "5") int size,
                                  @RequestParam(required = false, defaultValue = "name") String sortBy,
                                  @RequestParam(defaultValue = "1") int sortOrder,
                                  @RequestParam(name = "username") Optional<String> username,
                                  @RequestParam(name = "email") Optional<String> email) {

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("sortOrder", sortOrder);

        var customersPageable = customerService.getAllCustomersByNameOrEmail(page, size, sortBy, sortOrder, username, email)
                .map(CustomerDTO::from);
        model.addAttribute("page", customersPageable);
        return "block-replacement/employee-customer-replace";
    }

    @GetMapping("/employees/customers-replace/search")
    public String searchClientsByNameOrEmail(Model model,
                                             @RequestParam(value = "page", defaultValue = "0") int page,
                                             @RequestParam(required = false, defaultValue = "5") int size,
                                             @RequestParam(name = "username") Optional<String> username,
                                             @RequestParam(name = "email") Optional<String> email) {
        var customersPageable = customerService.getAllCustomersByNameOrEmail(page, size, username, email)
                .map(CustomerDTO::from);
        model.addAttribute("page", customersPageable);
        return "block-replacement/employee-customer-replace";
    }


    @PostMapping("/employees/profile/changes/email")
    public String applyCustomerProfileChange(@AuthenticationPrincipal UserDetails userDetails,
                                             @Valid EmployeeChangeDTO employeeDTO, BindingResult bindingResult,
                                             RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String userName = userDetails.getUsername();
        employeeService.changeProfileSettings(userName, employeeDTO);
        return "redirect:/";
    }

    @PostMapping("/employees/profile/changes/full-name")
    public String applyCustomerProfileFullNameChange(@AuthenticationPrincipal UserDetails userDetails,
                                                     @Valid EmployeeChangeFullNameDTO employeeDTO, BindingResult bindingResult,
                                                     RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String userName = userDetails.getUsername();
        employeeService.changeProfileFullName(userName, employeeDTO);
        return "redirect:/";
    }

    @PostMapping("/employees/profile/changes/login")
    public String applyCustomerProfileLoginChange(@AuthenticationPrincipal UserDetails userDetails,
                                                  @Valid EmployeeChangeLoginDTO employeeDTO, BindingResult bindingResult,
                                                  RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String userName = userDetails.getUsername();
        employeeService.changeProfileLogin(userName, employeeDTO);
        return "redirect:/";
    }

    @PostMapping("/employees/profile/changes/number")
    public String applyCustomerProfileNumberChanges(@AuthenticationPrincipal UserDetails userDetails,
                                                    @Valid EmployeeChangeNumberDTO employeeDTO, BindingResult bindingResult,
                                                    RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }
        String username = userDetails.getUsername();
        employeeService.changeProfileNumberSettings(username, employeeDTO);
        return "redirect:/";
    }

    @PostMapping("/employees/profile/changePassword")
    public String changePassword(@AuthenticationPrincipal UserDetails userDetails,
                                 @Valid ChangePasswordDTO changePasswordDTO,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {

        String username = userDetails.getUsername();
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        if (changePasswordDTO.getNewPassword().equals(changePasswordDTO.getNewPasswordRepeat())
                && employeeService.passwordCheck(username, changePasswordDTO.getPrevPassword())) {
            employeeService.changePassword(username, changePasswordDTO);
            redirectAttributes.addFlashAttribute("passwordChanged", true);
        } else {
            redirectAttributes.addFlashAttribute("passwordError", true);
        }

        return "redirect:/?error";
    }


    @GetMapping("/employees/new-request")
    public String getRequestCreatingPage(Model model,
                                         @RequestParam(required = false) String message,
                                         @RequestParam(required = false) String error) {
        model.addAttribute("message", message != null);
        model.addAttribute("error", error != null);
        var deliveries = deliveryService.getAll().stream()
                .map(DeliveryDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("deliveries", deliveries);
        return "employee-add-new-request";
    }

    @PostMapping("/employees/new-request")
    public Object getRequestCreatingPage(@Valid ManagerRequestDTO request,
                                         BindingResult bindingResult,
                                         Model model,
                                         @RequestParam(required = false) String message,
                                         @RequestParam(required = false) String error,
                                         @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("message", message != null);
        var deliveries = deliveryService.getAll().stream()
                .map(DeliveryDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("deliveries", deliveries);
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
            model.addAttribute("error", error != null);
            return "employee-add-new-request";
        }
        try {
            String employeeEmail = userDetails.getUsername();
            requestService.addNewManagerRequest(request, employeeEmail);
            return "redirect:/employees/new-request?message";
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при добавлении заявки: " + e.getMessage());
        }
    }

    @GetMapping("/employees/payments-replace")
    public String getPaymentDetails(@AuthenticationPrincipal UserDetails userDetails,
                                    Model model,
                                    @RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(required = false, defaultValue = "5") int size,
                                    @RequestParam(required = false, defaultValue = "request") String sortBy,
                                    @RequestParam(defaultValue = "1") int sortOrder) {
        String email = userDetails.getUsername();
        var dtoPage = orderService.getEmployeePayments(email, page, size, sortBy, sortOrder);

        if (sortBy.equals("request")) {
            model.addAttribute("page", dtoPage);
        }
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("sortOrder", sortOrder);

        return "block-replacement/employee-payments-details-replace";
    }
}
