package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.RegistrationDTO;
import kg.attractor.esdp.mercsgroup.entities.Csrf;
import kg.attractor.esdp.mercsgroup.services.CsrfService;
import kg.attractor.esdp.mercsgroup.services.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping("/register")
public class RegisterController {

    private final RegistrationService registrationService;
    private final CsrfService csrfService;

    @GetMapping("/generate-csrf-link")
    public ResponseEntity<String> generateCsrfLink(
            @RequestParam("c") String csrfToken,
            HttpServletRequest request
    ) {
        Csrf csrf = csrfService.generateCsrfLink(csrfToken);

        String domainName = request.getRequestURL().toString();
        int lastSlashIndex = domainName.lastIndexOf("/");
        if (lastSlashIndex > 0) {
            domainName = domainName.substring(0, lastSlashIndex);
        }
        String csrfLinkUrl = domainName + "?c=" + csrf.getCsrfToken();

        return ResponseEntity.ok(csrfLinkUrl);
    }

    @GetMapping
    public String registerPage(@RequestParam(value = "c", required = false) String csrfToken, Model model) {
        var csrf = csrfService.getCsrfByToken(csrfToken);
        if(csrf.isEmpty()){
            return "error/registerFail";
        }
        String[] parts = csrf.get().getCsrfToken().split("---");
        model.addAttribute("csrf", parts[0]);
        model.addAttribute("csrfToken", csrfToken);
        boolean isValid = csrfService.validateCsrf(csrfToken);
        if (isValid) {
            return "register";
        } else {
            return "error/registerFail";
        }
    }

    @PostMapping
    public String registration(@Valid RegistrationDTO data, BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            var csrf = csrfService.getCsrfByToken(data.getCsrfToken());
            String[] parts = csrf.get().getCsrfToken().split("---");
            model.addAttribute("csrf", parts[0]);
            model.addAttribute("csrfToken", data.getCsrfToken());
            model.addAttribute("bindingResult", bindingResult);
            return "register";
        } else {
            if (registrationService.registerNewCustomer(data, model)) {
                csrfService.deleteCsrf(data.getCsrfToken());
                return "redirect:/login";
            }
        }
        return "register";
    }
}
