package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.services.CsrfService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class CsrfController {
    private final CsrfService csrfService;

    @GetMapping("/csrf-token")
    public ResponseEntity<String> getCsrfToken(CsrfToken csrfToken) {
        String token = csrfToken.getToken();
        UUID uuid = UUID.randomUUID();
        return ResponseEntity.ok(token + "---" + uuid);
    }
}
