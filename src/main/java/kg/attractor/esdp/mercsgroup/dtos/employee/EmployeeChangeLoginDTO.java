package kg.attractor.esdp.mercsgroup.dtos.employee;

import kg.attractor.esdp.mercsgroup.validators.UniqueLogin;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class EmployeeChangeLoginDTO {
    private Long id;
    @NotBlank
    @NotEmpty
    @UniqueLogin
    private String login;
}
