package kg.attractor.esdp.mercsgroup.dtos.authorization;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
public class ChangePasswordDTO {
    @NotEmpty
    @NotBlank
    private String prevPassword;
    @NotEmpty
    @NotBlank
    @Size(min = 8, max = 24)
    private String newPassword;
    @NotEmpty
    @NotBlank
    @Size(min = 8, max = 24)
    private String newPasswordRepeat;
}
