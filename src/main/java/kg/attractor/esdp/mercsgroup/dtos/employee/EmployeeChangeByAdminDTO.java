package kg.attractor.esdp.mercsgroup.dtos.employee;

import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.validators.UniqueEmail;
import kg.attractor.esdp.mercsgroup.validators.UniqueINN;
import kg.attractor.esdp.mercsgroup.validators.UniqueLogin;
import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class EmployeeChangeByAdminDTO {
    public static EmployeeChangeByAdminDTO from(Employee employee){
        return builder()
                .id(employee.getId())
                .email(employee.getEmail())
                .fullName(employee.getFullName())
                .login(employee.getLogin())
                .inn(employee.getInn())
                .post(employee.getPost())
                .number(employee.getNumber())
                .build();

    }
    private Long id;
    private String fullName;
    @UniqueLogin
    private String login;
    @UniqueEmail
    private String email;
    @UniqueINN
    private String inn;
    private String post;
    @UniquePhone
    private String number;
    private boolean enabled;
    private String password;
    private Long roleId;
    private List<OrderDTO> orders;
}