package kg.attractor.esdp.mercsgroup.dtos.authorization;

import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Validated
@Builder
public class PasswordRecoveryDTO {
    @NotBlank(message = "Введите ваш email")
    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 16, message = "Пароль должен содержать минимум 8 символов")
    private String password;

    @NotBlank
    @Size(min = 8, max = 16, message = "Пароль должен содержать минимум 8 символов")
    private String confirmPassword;

    @AssertTrue(message = "Пароль и его подтверждение должны совпадать")
    public boolean isPasswordMatch() {
        return password.equals(confirmPassword);
    }
}
