package kg.attractor.esdp.mercsgroup.dtos.detail;

import kg.attractor.esdp.mercsgroup.entities.Detail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Validated
public class DetailDTO {

    public static DetailDTO from(Detail detail){
        return builder()
                .id(detail.getId())
                .order(detail.getOrder().getId())
                .productUrl(detail.getProductUrl())
                .quantity(detail.getQuantity())
                .quantityUnit(detail.getQuantityUnit())
                .productPrice(detail.getProductPrice())
                .freight(detail.getFreight())
                .customsValue(detail.getCustomsValue())
                .build();
    }

    private Long id;
    private Long order;
    @NotBlank(message = "Заполните ссылку")
    private String productUrl;
    @NotNull(message = "Заполните количество")
    @PositiveOrZero(message = "Значение количества не может быть меньше 0")
    private Double quantity;
    @NotBlank(message = "Введите единицу измерения")
    private String quantityUnit;
    @NotNull(message = "Заполните цену продукта")
    @PositiveOrZero(message = "Значение цены не может быть меньше 0")
    private Double productPrice;
    @NotNull(message = "Заполните фрахт")
    @PositiveOrZero(message = "Значение фрахта не может быть меньше 0")
    private Double freight;
    @NotNull(message = "Заполните Таможенную стоимость")
    @PositiveOrZero(message = "Значение таможней стоимости не может быть меньше 0")
    private Double customsValue;
}
