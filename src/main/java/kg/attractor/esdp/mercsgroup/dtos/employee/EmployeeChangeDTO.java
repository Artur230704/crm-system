package kg.attractor.esdp.mercsgroup.dtos.employee;

import kg.attractor.esdp.mercsgroup.validators.UniqueEmail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class EmployeeChangeDTO {
    private Long id;

    @Email(message = "Введите ваш email")
    @UniqueEmail
    @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$", message = "Неверный формат email")
    private String email;
}
