package kg.attractor.esdp.mercsgroup.dtos.employee;

import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeDTO {
    public static EmployeeDTO from(Employee employee){
        return builder()
                .id(employee.getId())
                .email(employee.getEmail())
                .fullName(employee.getFullName())
                .login(employee.getLogin())
                .inn(employee.getInn())
                .post(employee.getPost())
                .number(employee.getNumber())
                .build();

    }
    private Long id;
    private String fullName;
    private String login;
    private String email;
    private String inn;
    private String post;
    private String number;
    private boolean enabled;
    private String password;
    private Long roleId;
    private List<OrderDTO> orders;
}
