package kg.attractor.esdp.mercsgroup.dtos.authorization;


import kg.attractor.esdp.mercsgroup.validators.UniqueEmail;
import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Validated
@Builder
public class GuestRegistrationDTO {

    @Email
    @UniqueEmail
    private String email;

    @NotBlank(message = "Введите ваше имя")
    private String name;
    private String company;

    @UniquePhone
    @NotBlank(message = "Введите номер телефона")
    @Pattern(regexp = "^(\\+996\\s?)?(\\d{3}\\s?)?\\d{6}$", message = "Введите номер телефона в формате: +996000000000")
    private String phone;
}
