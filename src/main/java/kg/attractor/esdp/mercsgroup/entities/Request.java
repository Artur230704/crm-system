package kg.attractor.esdp.mercsgroup.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "requests")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    private String product;
    private String description;
    @Column(name = "request_date")
    private LocalDateTime requestDate;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;
    @ManyToOne
    @JoinColumn(name = "delivery_id")
    private Delivery delivery;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "request")
    @OrderBy("fileName ASC")
    private List<RequestFile> files;
    private String quantity;
    @Column(name="other_information")
    private String otherInformation;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "request")
    @OrderBy("orderDate ASC")
    private List<Order> orders;


}
