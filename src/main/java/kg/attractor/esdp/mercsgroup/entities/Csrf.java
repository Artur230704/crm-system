package kg.attractor.esdp.mercsgroup.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "csrf")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Csrf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "csrf_token")
    private String csrfToken;
    @Column(name = "created_date_time")
    private LocalDateTime createdDateTime;
    @Column(name = "expiration_date_time")
    private LocalDateTime expirationDateTime;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
}
