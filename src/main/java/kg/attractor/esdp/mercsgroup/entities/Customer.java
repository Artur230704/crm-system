package kg.attractor.esdp.mercsgroup.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customers")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
    @Column(name = "customer_data")
    private String customerData;
    private String email;
    private String name;
    private String company;
    private String post;
    private String number;
    private String password;
    private boolean enabled;

    @Column(name = "orders_quantity")
    private Integer ordersQuantity;
    @ManyToOne
    @JoinColumn(name = "activity_id")
    private Activity activity;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @OrderBy("orderDate ASC")
    private List<Order> orders;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
    @OrderBy("requestDate ASC")
    private List<Request> requests;

}
