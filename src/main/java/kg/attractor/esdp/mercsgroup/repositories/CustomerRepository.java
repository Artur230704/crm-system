package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Activity;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {

    Optional<Customer> findByEmail(String email);

    Optional<Customer> getCustomerById(Long customerId);

    @Query("select c from Customer as c order by c.name asc")
    Page<Customer> getAllCustomersByPageable(Pageable pageable);
    Page<Customer> findCustomersBy(Pageable pageable);

    default Page<Customer> searchByNameAndEmailAndPageable(Optional<String> name, Optional<String> email, Pageable pageable) {
        return findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            name.ifPresent(s -> {
                if (!s.isBlank()) {
                    predicates.add(criteriaBuilder.like(root.get("name"), "%" + s + "%"));
                }
            });

            email.ifPresent(s -> {
                if (!s.isBlank()) {
                    predicates.add(criteriaBuilder.like(root.get("email"), "%" + s + "%"));
                }
            });

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    Page<Customer> findAll(Pageable pageable);

    Page<Customer> findCustomersByRoleRole(String role, Pageable pageable);

    Optional<Customer> findByNumber(String number);
    Optional<Customer> findByEmailOrNumber(String email, String number);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Customer c SET c.password = :password WHERE c.id = :id")
    void setPasswordById(@Param("password") String password, @Param("id") Long id);

    @Query(value = "select c from Customer as c where (:username is null or c.name like %:username%) and (:email is null or c.email like %:email%) order by :singleValue")
    Page<Customer> searchByNameAndEmailOrderByOne(@Param("username") Optional<String> username,
                                                  @Param("email") Optional<String> email,
                                                  @Param("singleValue") String singleValue, Pageable pageable);

    @Query(value = "select c from Customer as c where (:username is null or c.name like %:username%) and (:email is null or c.email like %:email%) order by :nameSort, :emailSort")
    Page<Customer> searchByNameAndEmailOrderByTwo(@Param("username") Optional<String> username,
                                                  @Param("email") Optional<String> email,
                                                  String nameSort, String emailSort, Pageable pageable);

    boolean existsByEmail(String email);

    boolean existsByNumber(String email);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Customer c SET c.name = :name, c.email = :email, c.role = :role, " +
            "c.activity = :activity, c.post = :post, c.company = :company WHERE c.id = :id")
    void updateCustomerFieldsById(@Param("name") String name, @Param("email") String email,
                                  @Param("role") Role role, @Param("activity") Activity activity,
                                  @Param("post") String post, @Param("company") String company,
                                  @Param("id") Long id);


}
