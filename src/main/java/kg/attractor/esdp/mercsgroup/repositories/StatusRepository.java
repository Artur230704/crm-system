package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

    Optional<Status> findByStatus(Status status);

    Optional<Status> findStatusByStatus(String status);

}
