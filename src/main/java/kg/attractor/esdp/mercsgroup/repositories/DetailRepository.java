package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Detail;
import kg.attractor.esdp.mercsgroup.entities.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Long> {

    Page<Detail> findAllByOrder(Order order, Pageable pageable);

    @Query("select d from Detail d where d.order.id = :orderId")
    Page<Detail> findAllByOrderId(@Param("orderId") Long orderId, Pageable pageable);

    @Query("delete from Detail d where d.order.id =:orderId")
    void deleteAllDetailsByOrderId(Long orderId);
}
