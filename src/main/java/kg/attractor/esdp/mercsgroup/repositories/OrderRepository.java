package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {


    @Query("select o from Order o where o.customer.email = :email")
    Page<Order> findAllByCustomerEmail(@Param("email") String email, Pageable pageable);

    Page<Order> findAllByCustomerEmailAndStatusIdGreaterThan(String email, Long statusId, Pageable pageable);

    int countAllByCustomerId(Long customerId);

    boolean existsOrderByIdAndEmployeeId(Long orderId, Long customerId);
    Page<Order> findOrdersByCustomerId(Long customerIid, Pageable pageable);

    @Query("select o from Order o where o.employee.email = :email")
    Page<Order> findAllByEmployeesEmail(@Param("email") String email, Pageable pageable);

    Page<Order> findAllByEmployeeEmailAndStatusIdGreaterThan(String email,Long statusId, Pageable pageable);

    @Query("select o from Order o where o.employee.email = :email")
    List<Order> findAllByEmployeesEmail(String email);

    Optional<Order> findOrderById(Long id);

    default Page<Order> searchByProductAndDescriptionAndStatus(Optional<String> product, Optional<String> description,
                                                               Optional<String> status, Long customerId, Pageable pageable) {
        return findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            product.ifPresent(p -> {
                if (StringUtils.hasText(p)) {
                    predicates.add(criteriaBuilder.like(root.join("request").get("product"), "%" + p + "%"));
                }
            });

            description.ifPresent(d -> {
                if (StringUtils.hasText(d)) {
                    predicates.add(criteriaBuilder.like(root.join("request").get("description"), "%" + d + "%"));
                }
            });

            status.ifPresent(s -> {
                if (!s.equalsIgnoreCase("все")) {
                    if (StringUtils.hasText(s)) {
                        predicates.add(criteriaBuilder.equal(root.join("status").get("status"), s));
                    }
                }
            });

            predicates.add(criteriaBuilder.equal(root.join("customer").get("id"), customerId));


            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    default Page<Order> searchOrderByOrderedAndSortedThreeValues(Optional<String> productName, Optional<String> description,
                                                                 Optional<String> status, Long customerId, Pageable pageable) {
        return findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            productName.ifPresent(p -> {
                if (StringUtils.hasText(p)) {
                    predicates.add(criteriaBuilder.like(root.join("request").get("product"), "%" + p + "%"));
                }
            });
            description.ifPresent(d -> {
                if (StringUtils.hasText(d)) {
                    predicates.add(criteriaBuilder.like(root.join("request").get("description"), "%" + d + "%"));
                }
            });
            status.ifPresent(s -> {
                if (!s.equalsIgnoreCase("все")) {
                    if (StringUtils.hasText(s)) {
                        predicates.add(criteriaBuilder.equal(root.join("status").get("status"), s));
                    }
                }
            });
            predicates.add(criteriaBuilder.equal(root.join("customer").get("id"), customerId));

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    default Page<Order> searchOrderCustomersByOrdered(Optional<String> customerName, Optional<String> request,
                                                      Optional<String> status, Pageable pageable, String email) {
        return findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.hasText(email)) {
                predicates.add(criteriaBuilder.like(root.join("employee").get("email"), email));
            }

            customerName.ifPresent(c -> {
                if (StringUtils.hasText(c)) {
                    predicates.add(criteriaBuilder.like(root.join("customer").get("name"), "%" + c + "%"));
                }
            });

            request.ifPresent(m -> {
                if (StringUtils.hasText(m)) {
                    predicates.add(criteriaBuilder.like(root.join("request").get("product"), "%" + m + "%"));
                }
            });

            status.ifPresent(s -> {
                if (!s.equalsIgnoreCase("Все") && StringUtils.hasText(s)) {
                    predicates.add(criteriaBuilder.equal(root.join("status").get("status"), s));
                }
            });
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }

    @Query("select o.id from Order o " +
            "where o.employee.email like :email and o.leadEnd < :currentDate ")
    List<Long> findOrdersWithAnExpiredLead(String email, LocalDateTime currentDate);
}
