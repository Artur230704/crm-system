package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.utils.SortUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.DoubleStream;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    public Customer findCustomer(String email) {
        return customerRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Customer not found"));
    }

    public Page<Customer> getAllCustomersByPageable(Integer page, Integer size, String sortBy, int sortOrder) {
        page = getPage(page);
        Pageable pageable;

        if (sortOrder == 1){
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));

        }
        return customerRepository.findCustomersBy(pageable);
    }

    private Integer getPage(Integer page) {
        if (page != null && page > 0) {
            return page - 1;
        }
        return 0;
    }

    public Page<Customer> getAllCustomersByNameOrEmail(Integer page, Integer size, String sortBy, int sortOrder, Optional<String> name,
                                                       Optional<String> email) {

        page = getPage(page);
        Pageable pageable;

        if (sortOrder == 1) {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));
        }
        return customerRepository.searchByNameAndEmailAndPageable(name, email, pageable);
    }

    public void applyCustomerProfileChanges(Customer customer, CustomerDTO customerDTO) {

        if (!customerDTO.getName().isEmpty()) customer.setName(customerDTO.getName());
        if (!customerDTO.getNumber().isEmpty()) customer.setNumber(customerDTO.getNumber());
        if (!customerDTO.getCompany().isEmpty()) customer.setCompany(customerDTO.getCompany());
        if (!customerDTO.getPost().isEmpty()) customer.setPost(customerDTO.getPost());

        customerRepository.save(customer);
    }

    public boolean passwordCheck(String username, String prevPassword) {
        Customer customer = findCustomer(username);
        return passwordEncoder.matches(prevPassword, customer.getPassword());
    }

    public void changePassword(String username, ChangePasswordDTO customerDTO) {
        Customer customer = findCustomer(username);
        customer.setPassword(passwordEncoder.encode(customerDTO.getNewPassword()));
        customerRepository.save(customer);
    }


    public void changeProfileSettings(String userName, CustomerChangeDTO customerChangeDTO) {
        Customer customer = findCustomer(userName);

        if (!customerChangeDTO.getName().isEmpty()) customer.setName(customerChangeDTO.getName());
        if (!customerChangeDTO.getEmail().isEmpty()) customer.setEmail(customerChangeDTO.getEmail());
        if (!customerChangeDTO.getPost().isEmpty()) customer.setPost(customerChangeDTO.getPost());
        if (!customerChangeDTO.getCompany().isEmpty()) customer.setCompany(customerChangeDTO.getCompany());

        customerRepository.save(customer);
    }

    public void changeProfileNumberSettings(String username, CustomerChangeNumberDTO customerChangeNumberDTO) {
        Customer customer = findCustomer(username);
        if (!customerChangeNumberDTO.getNumber().isEmpty()) customer.setNumber(customerChangeNumberDTO.getNumber());
        customerRepository.save(customer);
    }

    public Page<Customer> getAllCustomersByNameOrEmail(int page, int size, Optional<String> name, Optional<String> email) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return customerRepository.searchByNameAndEmailAndPageable(name, email, pageable);
    }
}