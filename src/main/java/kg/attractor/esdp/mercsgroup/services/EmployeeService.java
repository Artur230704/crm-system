package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeFullNameDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeLoginDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder passwordEncoder;

    public Employee findEmployee(String email) {
        return employeeRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Employee not found"));
    }

    public boolean isLoginExists(String s) {
        return employeeRepository.existsByLogin(s);
    }

    public boolean isINNExists(String s) {
        return employeeRepository.existsByInn(s);
    }

    public boolean isPhoneExists(String s) {
        return employeeRepository.existsByNumber(s);
    }

    public void changeProfileSettings(String username, EmployeeChangeDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getEmail().isEmpty()) employee.setEmail(employeeDTO.getEmail());

        employeeRepository.save(employee);
    }

    public boolean passwordCheck(String username, String prevPassword) {
        Employee employee = findEmployee(username);
        return passwordEncoder.matches(prevPassword, employee.getPassword());
    }

    public void changePassword(String username, ChangePasswordDTO changePasswordDTO) {
        Employee employee = findEmployee(username);
        employee.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
        employeeRepository.save(employee);
    }

    public boolean isEmailExists(String email) {
        return employeeRepository.existsByEmail(email);
    }

    public void changeProfileNumberSettings(String username, EmployeeChangeNumberDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getNumber().isEmpty()) employee.setNumber(employeeDTO.getNumber());
        employeeRepository.save(employee);
    }

    public void changeProfileLogin(String userName, EmployeeChangeLoginDTO employeeDTO) {
        Employee employee = findEmployee(userName);
        if (!employeeDTO.getLogin().isEmpty()) employee.setLogin(employeeDTO.getLogin());
        employeeRepository.save(employee);
    }

    public void changeProfileFullName(String userName, EmployeeChangeFullNameDTO employeeDTO) {
        Employee employee = findEmployee(userName);
        if (!employeeDTO.getFullName().isEmpty()) employee.setFullName(employeeDTO.getFullName());
        employeeRepository.save(employee);
    }
}
