package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Csrf;
import kg.attractor.esdp.mercsgroup.repositories.CsrfRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CsrfService {
    private static final int EXPIRATION_HOURS = 1;
    private final CsrfRepository csrfRepository;

    public Csrf generateCsrfLink(String csrfToken) {
        LocalDateTime createdDateTime = LocalDateTime.now();
        LocalDateTime expirationDateTime = createdDateTime.plusHours(EXPIRATION_HOURS);

        Csrf csrf = new Csrf();
        csrf.setCsrfToken(csrfToken);
        csrf.setCreatedDateTime(createdDateTime);
        csrf.setExpirationDateTime(expirationDateTime);

        csrfRepository.deleteAllByExpirationDateTimeBefore(createdDateTime);
        return csrfRepository.save(csrf);
    }

    public boolean validateCsrf(String csrfToken) {
        var csrf = csrfRepository.findByCsrfToken(csrfToken);

        if (csrf.isPresent()) {
            LocalDateTime currentDate = LocalDateTime.now();

            if (currentDate.isBefore(csrf.get().getExpirationDateTime())) {
                return true;
            } else {
                csrfRepository.delete(csrf.get());
                return false;
            }
        }
        return false;
    }

    public Optional<Csrf> getCsrfByToken(String csrfToken) {
        return csrfRepository.findByCsrfToken(csrfToken);
    }

    public void deleteCsrf(String csrfToken) {
        var csrf = csrfRepository.findByCsrfToken(csrfToken);
        csrf.ifPresent(csrfRepository::delete);
    }
}
