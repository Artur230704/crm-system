'use strict';

const newRequest = document.getElementById('new-request');
newRequest.addEventListener('submit',addNewRequest);
const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
const csrfToken = document.querySelector('meta[name="_csrf_token"]').content;

async function addNewRequest(e){
    e.preventDefault();
    const data = new FormData(e.target);
    const response = await fetch('/requests/new', {
        method: 'POST',
        headers: {
            [csrfHeader]: csrfToken
        },
        body: data
    });
    if (response.ok) {
        window.location.href='?message';
    } else {
        window.location.href='?error';
    }
    e.target.reset();
}