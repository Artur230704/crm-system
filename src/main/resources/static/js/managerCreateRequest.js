'use strict'

const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
const csrfToken = document.querySelector('meta[name="_csrf_token"]').content;

const maxFiles = 10;

let fileCounter = 1;

function addFileInput() {
    fileCounter++;
    const newFileInput = document.createElement("input");
    newFileInput.type = "file";
    newFileInput.name = "files[]";
    newFileInput.className = "form-control my-2";
    const label = document.createElement("label");
    label.innerText = `Выберите файл №:`+fileCounter;
    label.className = "form-label";

    const container = document.getElementById("attach-file");
    container.appendChild(label);
    container.appendChild(newFileInput);

    if (fileCounter === maxFiles) {
        document.getElementById("add-attachment-button").style.display = "none";
    }
}