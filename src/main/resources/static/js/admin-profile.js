const BASIC_URL = '/admin';
function change(id) {
    const elements = ['information','customers', 'guests', 'employees', 'settings'];
    elements.forEach(elementId => {
        const element = document.getElementById(elementId);
        element.style.display = elementId === id ? 'block' : 'none';
    });
}
const currentURL = window.location.href;


if (currentURL.includes("/?error")) {
    // Выполняем определенный метод
    change('settings')
}

async function getCurrentBlock(type) {
    const response = await fetch(`${BASIC_URL}/${type}-replace`);
    const html = await response.text();
    if (ifLogin(html)) {
        window.location.href = "/login";
        return;
    }
    const block = document.getElementById(type);
    block.innerHTML = html;
    change(type)
}

function ifLogin(html) {
    const block = document.createElement('div');
    block.innerHTML = html;
    const itsLoginElement = block.querySelector('#itsLogin');
    return !!itsLoginElement;

}

async function changePage(page, count, type) {
    const newPage = parseInt(page) + count;
    const response = await fetch(`${BASIC_URL}/${type}-replace?page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById(type);
    block.innerHTML = html;
}

async function sortPage(event) {
    event.preventDefault();

    const checkboxes = document.querySelectorAll('input[type="checkbox"]:checked');
    const selectedValues = Array.from(checkboxes).map(checkbox => checkbox.value);

    const queryParams = new URLSearchParams({sort: selectedValues}).toString();
    const url = `${BASIC_URL}/customers-replace/sort?${queryParams}`;

    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('customers');
    block.innerHTML = html;
}
async function searchOrders(event){
    event.preventDefault();

    const form = document.getElementById('searchForm');
    const productInput = form.elements.customer;
    const descriptionInput = form.elements.manager;
    const statusInput = form.elements.status;

    const productValue = productInput ? productInput.value : '';
    const descriptionValue = descriptionInput ? descriptionInput.value : '';
    const statusValue = statusInput ? statusInput.value : '';

    const url = `${BASIC_URL}/orders-replace/search?customer=${productValue}&manager=${descriptionValue}&status=${statusValue}`;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('orders');
    block.innerHTML = html;
}

async function searchClients(event) {
    event.preventDefault();

    const form = document.getElementById('searchForm');
    const nameInput = form.elements.name;
    const emailInput = form.elements.email;

    const nameValue = nameInput.value;
    const emailValue = emailInput.value;

    const url = `${BASIC_URL}/customers-replace/search?username=${nameValue}&email=${emailValue}`;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('customers');
    block.innerHTML = html;
}

function openModal(name, customerData, email, company, post, number, address) {
    let modal = new bootstrap.Modal(document.getElementById('customer-modal'));
    let modalBody = document.getElementById('modalBody');

    modalBody.innerHTML = `
        <h5 class="mb-5">Клиент: ${name}</h5>
        <p>Дополнительная информация: ${customerData}</p>
        <p>Почта: ${email}</p>
        <p>Компания: ${company}</p>
        <p>Должность: ${post}</p>
        <p>Телефон: ${number}</p>
        <p>Адрес: ${address}</p>
    `;
    modal.show();

    let closeButton = document.querySelector('.modal-header .btn-close');
    closeButton.addEventListener('click', () => {
        modal.hide();
    });
}


function openOrderModal(element) {
    let modal = new bootstrap.Modal(document.querySelector(`#${element}`));
    modal.show();

    let closeButton = document.querySelector(`#${element + '-btn-close'}`)
    closeButton.addEventListener('click', () => {
        modal.hide();
    });
}

function showDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'block';
}

function closeDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'none';
}

const sendChangeButton = document.getElementById('admin-customer-edit-form');
if (sendChangeButton) {
    sendChangeButton.addEventListener('submit', sendChangeEmployee);
}
async function sendChangeEmployee(e) {
    e.preventDefault();
    const data = new FormData(e.target);
    const csrfTokenElement = document.getElementById("csrf-token-ids");
    const csrfParamName = csrfTokenElement.getAttribute("name");
    const csrfToken = csrfTokenElement.getAttribute("value");
    const res = await fetch("/admin/employee", {
        method: 'POST',
        headers: {
            [csrfParamName]: csrfToken
        },
        body: data
    });
    if (res.ok) {
        let responseField = document.getElementById('response-field')
        responseField.classList.remove('d-none')
    } else {
        const errorResponse = await res.json();
        if (errorResponse && errorResponse.errors) {
            const errorMessages = errorResponse.errors;
            errorMessages.forEach((error) => {
                let message = error.defaultMessage;
                let field = error.field;
                let errorField = document.getElementById(`${field}-error`)
                errorField.innerHTML = message
                errorField.classList.remove('d-none')
            })
        } else {
            alert('Произошла ошибка');
        }
    }
    e.target.reset();
}