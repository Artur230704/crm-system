'use strict';

document.addEventListener('DOMContentLoaded', function () {
    const button = document.getElementById('loadingButton');
    const emailInput = document.getElementById('email-input-recovery');
    button.addEventListener('click', function () {
        const emailValue = emailInput.value.trim();
        const originalText = button.innerHTML;
        if (emailValue !== "") {
            const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
            const csrfToken = document.querySelector('meta[name="_csrf_token"]').content;
            button.disabled = true;
            button.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Загрузка...';
            const formData = new FormData();
            formData.append('username', emailValue);
            fetch('/password-recovery/specify-email', {
                method: 'POST',
                headers: {
                    [csrfHeader]: csrfToken
                },
                body: formData
            }).catch(error => {
                console.error('Ошибка при выполнении запроса:', error);
                button.innerHTML = originalText;
            })
            button.disabled = true;
            button.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Загрузка...';
            setTimeout(function () {
                button.disabled = false;
                button.innerHTML = originalText;
            }, 10000);
        }
    });
});