'use strict';

const BASIC_URL = '/employees';
let CURRENT_PAGE = 0;
let SORT_ORDER = 1;


function change(id) {
    const elements = ['orders', 'customers', 'payments', 'settings',
        'requests', 'links', 'information', 'csv'];
    elements.forEach(elementId => {
        const element = document.getElementById(elementId);
        element.style.display = elementId === id ? 'block' : 'none';
    });
}

const currentURL = window.location.href;


if (currentURL.includes("/?error")) {
    change('settings')
} else {
    getCurrentBlock('orders')
}

async function getCurrentBlockAndClearLS(type){
    localStorage.clear();
    await getCurrentBlock(type);
}

async function getCurrentBlock(type) {
    const response = await fetch(`${BASIC_URL}/${type}-replace`);
    const html = await response.text();
    if (ifLogin(html)) {
        window.location.href = "/login";
        return;
    }
    const block = document.getElementById(type);
    block.innerHTML = html;
    change(type)
}

function ifLogin(html) {
    const block = document.createElement('div');
    block.innerHTML = html;
    const itsLoginElement = block.querySelector('#itsLogin');
    return !!itsLoginElement;
}

function clickToChange(num) {
    const sendChangeDetailsButton = document.getElementById(`change-details-form-${num}`);
    if (sendChangeDetailsButton) {
        sendChangeDetailsButton.addEventListener('submit', changeOrderDetails);
    }
}

async function changeOrderDetails(e) {
    e.preventDefault();
    const data = new FormData(e.target);
    const csrfTokenElement = document.getElementById("csrf-token-id-details");
    const csrfParamName = csrfTokenElement.getAttribute("name");
    const csrfToken = csrfTokenElement.getAttribute("value");
    await fetch("/orders/change", {
        method: 'POST',
        headers: {
            [csrfParamName]: csrfToken
        },
        body: data
    }).then((response) => {
        if (!response.ok) {
            return response.json().then((errorData) => {
                for (const error of errorData) {
                    const field = error.field;
                    const message = error.message;
                    const fieldSplit = field.split('.')[1];
                    let errorField = document.getElementById(`${fieldSplit}-error-${data.get('id')}`)
                    errorField.innerHTML = message
                    errorField.classList.remove('d-none')
                }
            });
        } else {
            const p = '<div class="alert alert-info" role="alert">Успешно изменен</div>';
            const div = document.getElementById(`order-edit-${data.get('id')}-modal`);
            const h5Element = div.querySelector('h5');
            if (h5Element) {
                h5Element.innerHTML = p;
            }
            e.target.reset();
            window.location.reload();
        }
    })
        .catch((error) => {
            console.error('Error:', error);
        });
}

async function changeOrdersPage(page, count) {
    CURRENT_PAGE = page;
    if (count > 0) {
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }
    const productValue = localStorage.getItem('productValue') || '';
    const descriptionValue = localStorage.getItem('descriptionValue') || '';
    const statusValue = localStorage.getItem('statusValue') || '';
    const sortParam = localStorage.getItem('sortParam') || '';

    const newPage = parseInt(page) + count;
    const response = await fetch(`${BASIC_URL}/orders-replace?sortBy=${sortParam}&sortOrder=${SORT_ORDER}&customer=${productValue}&manager=${descriptionValue}&status=${statusValue}&page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById('orders');
    block.innerHTML = html;
}


async function changePage(page, count, type) {
    CURRENT_PAGE = page;
    if (count > 0) {
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }
    const newPage = parseInt(page) + count;
    const response = await fetch(`${BASIC_URL}/${type}-replace?page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById(type);
    block.innerHTML = html;
}
async function changePaymentsPage(page, count) {
    CURRENT_PAGE = page;
    if (count > 0) {
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }
    const newPage = parseInt(page) + count;
    const sortParam = localStorage.getItem('sortParam') || '';
    const response = await fetch(`${BASIC_URL}/payments-replace?page=${newPage}&sortBy=${sortParam}&sortOrder=${SORT_ORDER}`);
    const html = await response.text();
    const block = document.getElementById('payments');
    block.innerHTML = html;
}

async function changeCustomersPage(page, count) {
    CURRENT_PAGE = page;
    if (count > 0) {
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }
    const newPage = parseInt(page) + count;
    const emailValue = localStorage.getItem('emailValue') || '';
    const nameValue = localStorage.getItem('nameValue') || '';
    const sortParam = localStorage.getItem('sortParam') || '';
    const response = await fetch(`${BASIC_URL}/customers-replace?sortBy=${sortParam}&sortOrder=${SORT_ORDER}&username=${nameValue}&email=${emailValue}&page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById('customers');
    block.innerHTML = html;
}


async function searchClients(event) {
    event.preventDefault();

    const nameInput = document.getElementById('nameInput');
    const emailInput = document.getElementById('emailInput');
    const nameValue = nameInput ? nameInput.value : '';
    const emailValue = emailInput ? emailInput.value : '';

    localStorage.setItem('nameValue', nameValue);
    localStorage.setItem('emailValue', emailValue);

    const url = '/employees/customers-replace/search?username=' + nameValue + '&email=' + emailValue;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('customers');
    block.innerHTML = html;
}

function openModal(name, customerData, email, company, post, number, address) {
    let modal = new bootstrap.Modal(document.getElementById('customer-modal'));
    let modalBody = document.getElementById('clientModalBody');

    modalBody.innerHTML = `
        <h5 class="mb-5">Клиент: ${name}</h5>
        <p>Дополнительная информация: ${customerData}</p>
        <p>Почта: ${email}</p>
        <p>Компания: ${company}</p>
        <p>Должность: ${post}</p>
        <p>Телефон: ${number}</p>
        <p>Адрес: ${address}</p>
    `;

    modal.show();

    let closeButton = document.querySelector('.modal-header .btn-close');
    closeButton.addEventListener('click', () => {
        modal.hide();
    });
}

function openOrderModal(element) {
    clearSpace();
    let modal = new bootstrap.Modal(document.querySelector(`#${element}`));
    modal.show();

    let closeButton = document.querySelector(`#${element + '-btn-close'}`)
    closeButton.addEventListener('click', () => {
        modal.hide();
    });
}

function clearSpace() {
    const divs = document.getElementsByClassName('space-deleteIdClass');

    for (let j = 0; j < divs.length; j++) {
        const div = divs[j];
        const nodes = div.getElementsByTagName('*');
        for (let i = 0; i < nodes.length; i++) {
            const node = nodes[i];
            if (node.innerHTML.includes('&nbsp;')) {
                node.innerHTML = node.innerHTML.replace(/&nbsp;/g, '');
            }
        }
    }
}

function copyText() {
    fetch("/csrf-token").then(response => response.text())
        .then(data => {
            shareCsrf(data);
            const link = document.getElementById('copied-link');
            link.style.display = 'block';
            setTimeout(hideCopy, 3000);
        })
}

function shareCsrf(value) {
    fetch(`/register/generate-csrf-link?c=${value}`).then(response => response.text())
        .then(data => {
            const myInput = document.getElementById('copied-link-p2');
            myInput.value = data;
            myInput.style.display = 'block';
            myInput.select();
            navigator.clipboard.writeText(data)
                .catch(function (error) {
                    console.log(error);
                })
        })
}


function hideCopy() {
    const link = document.getElementById('copied-link');
    link.style.display = 'none';
}

function showDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'block';
}

function closeDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'none';
}

document.getElementById('csv-form').addEventListener('submit', function (event) {
    event.preventDefault();
    const form = event.target;
    const formData = new FormData(form);

    fetch(form.action, {
        method: form.method,
        body: formData
    })
        .then(function (response) {
            const fileInput = document.getElementById('file-input');
            fileInput.form.reset();
            if (response.ok) {
                showCSVUploadInfo('csv-upload');
            } else {
                showCSVUploadInfo('csv-upload-error')
            }
        })
        .catch(function (error) {
            console.log(error);
            showCSVUploadInfo('csv-upload-error');
        });
});

function showCSVUploadInfo(blockId) {
    const csvUploadElement = document.getElementById(blockId);
    csvUploadElement.style.display = 'block';

    setTimeout(function () {
        csvUploadElement.style.display = 'none';
    }, 3000);
}


function closeOrOpenModal(id) {
    const modal = document.getElementById(id);
    if (modal.style.display === 'none') {
        modal.style.display = 'block';
    } else {
        modal.style.display = 'none';
    }
}

function showCustomConfirm() {
    const customConfirm = document.getElementById('confirmOffer');
    const confirmBtn = document.getElementById('confirmBtn');
    const cancelBtnTop = document.getElementById('cancelBtnTop');
    const cancelBtn = document.getElementById('cancelBtn');

    customConfirm.style.display = 'flex';

    return new Promise((resolve) => {
        function confirmAction(confirm) {
            customConfirm.style.display = 'none';
            resolve(confirm);
        }

        confirmBtn.onclick = () => confirmAction(true);
        cancelBtn.onclick = () => confirmAction(false);
        cancelBtnTop.onclick = () => confirmAction(false);
    });
}

async function changeStatus(orderId) {
    showCustomConfirm(orderId).then((confirmed) => {
        if (confirmed) {
            updateOrderStatus(orderId);
        }
    });
}

async function updateOrderStatus(orderId) {

    let csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
    let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");

    const data = {
        orderId: orderId
    };


    fetch('/orders/status/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            if (response.ok) {
                getCurrentBlock('orders')
            } else {
                console.error('Error:', response.statusText);
            }
        })
}


async function searchOrders(event) {
    event.preventDefault();
    const form = document.getElementById('searchForm');
    const productInput = form.elements.customer;
    const descriptionInput = form.elements.manager;
    const statusInput = form.elements.status;
    const productValue = productInput ? productInput.value : '';
    const descriptionValue = descriptionInput ? descriptionInput.value : '';
    const statusValue = statusInput ? statusInput.value : '';

    localStorage.setItem('productValue', productValue);
    localStorage.setItem('descriptionValue', descriptionValue);
    localStorage.setItem('statusValue', statusValue);

    const url = `${BASIC_URL}/orders-replace/search-sort?customer=${productValue}&manager=${descriptionValue}&status=${statusValue}`;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('orders');
    block.innerHTML = html;
}


const OrdersSortBy = async (param) => {
    localStorage.setItem('sortParam', param);

    const productValue = localStorage.getItem('productValue') || '';
    const descriptionValue = localStorage.getItem('descriptionValue') || '';
    const statusValue = localStorage.getItem('statusValue') || '';

    changeSortOrder();

    const url = `${BASIC_URL}/orders-replace?sortBy=${param}&sortOrder=${SORT_ORDER}&customer=${productValue}&manager=${descriptionValue}&status=${statusValue}`;

    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('orders');
    block.innerHTML = html;
}

const customerSortBy = async (param) => {
    localStorage.setItem('sortParam', param);
    changeSortOrder();

    let nameValue = localStorage.getItem('nameValue') || '';
    let emailValue =  localStorage.getItem('emailValue') || '';

    const url = `${BASIC_URL}/customers-replace?sortBy=${param}&sortOrder=${SORT_ORDER}&username=${nameValue}&email=${emailValue}`;

    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('customers');
    block.innerHTML = html;
}

const changeSortOrder = () => {
    if (SORT_ORDER === -1) {
        SORT_ORDER = 1;
    } else {
        SORT_ORDER = -1;
    }
}

const paymentsSortBy = async (param) => {
    changeSortOrder();
    localStorage.setItem('sortParam', param);
    const url = `${BASIC_URL}/payments-replace?sortBy=${param}&sortOrder=${SORT_ORDER}`;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('payments');
    block.innerHTML = html;
}