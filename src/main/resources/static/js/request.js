'use strict'

const button = document.getElementById('change-request-details');
addEventListener('submit', changeRequestDetails);
const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
const csrfToken = document.querySelector('meta[name="_csrf_token"]').content;

async function deleteRequestById(id) {
    await fetch(`/requests/delete?id=${id}`, {
        method: 'POST',
        headers: {
            [csrfHeader]: csrfToken
        }
    });
    window.location.href = '/';
}

function closeAndOpenModal(idModal) {
    const divModal = document.getElementById(idModal);
    if (divModal.style.display === 'none') {
        divModal.style.display = 'block';
    } else {
        divModal.style.display = 'none';
    }
}

async function changeRequestDetails(e) {
    e.preventDefault();
    const storedValue = localStorage.getItem("images");
    if (storedValue) {
        const values = storedValue.split(',');
        await fetch(`/requests/delete/img?name=${values}`, {
            method: 'POST',
            headers: {
                [csrfHeader]: csrfToken
            }
        });
        localStorage.removeItem('images');
    }
    const data = new FormData(e.target);
    await fetch('/requests/change', {
        method: 'POST',
        headers: {
            [csrfHeader]: csrfToken
        },
        body: data
    }).then(response => {
        if (response.ok) {
            window.location.reload();
        } else {
            const p = `<div class="alert alert-danger" role="alert">Ошибка заполните поля</div>`;
            const div = document.getElementById('modal-title-elem');
            div.innerHTML = p;
        }
    })
}

const fileInput = document.getElementById('fileInput');
fileInput.addEventListener('change', function () {
    if (fileInput.files.length > 10) {
        alert('Максимальное количество файлов - 10');
        fileInput.value = '';
    }
});

function deleteImageByFilename(e, fileName) {
    let span = e.target;
    if (span.textContent === '❌') {
        span.textContent = '✔';
    } else {
        span.textContent = '❌';
    }
    const elem = document.getElementById(fileName);
    const idValue = elem.id;
    saveToLocalStorage(idValue);
}

function saveToLocalStorage(value) {
    const storedValue = localStorage.getItem("images");
    if (storedValue) {
        const values = storedValue.split(",");
        const index = values.indexOf(value);
        if (index !== -1) {
            values.splice(index, 1);
        } else {
            values.push(value);
        }
        localStorage.setItem("images", values.join(","));
    } else {
        localStorage.setItem("images", value);
    }
}

document.addEventListener('DOMContentLoaded', function () {
   localStorage.removeItem('images');
});