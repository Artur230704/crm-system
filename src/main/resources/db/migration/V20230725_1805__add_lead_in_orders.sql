alter table orders
    add lead varchar;

alter table orders
    add lead_start timestamp;

alter table orders
    add lead_end timestamp;

alter table orders
    add constraint lead_check
        check (lead in ('Холодный','Теплый','Горячий'));

alter table orders
    add constraint lead_end_check
        check (lead_end > lead_start);
