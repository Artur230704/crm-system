alter table details
    rename column price to product_price;

alter table details
    add percentage real;

alter table details
    add final_cost real;