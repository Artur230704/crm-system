ALTER TABLE tokens
    DROP CONSTRAINT IF EXISTS tokens_role_id_fkey,
    ADD CONSTRAINT tokens_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE;
