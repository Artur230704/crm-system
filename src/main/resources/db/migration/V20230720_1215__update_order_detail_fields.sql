alter table details
alter column quantity type real using quantity::real;

alter table details
    add quantity_unit varchar;
