UPDATE statuses
SET status = 'Новый'
WHERE id = 1;

UPDATE statuses
SET status = 'В обработке'
WHERE id = 2;

UPDATE statuses
SET status = 'Ожидается оплата'
WHERE id = 3;

UPDATE statuses
SET status = 'Ожидается подтверждение'
WHERE id = 4;

UPDATE statuses
SET status = 'В процессе'
WHERE id = 5;

UPDATE statuses
SET status = 'Отправлен'
WHERE id = 6;

UPDATE statuses
SET status = 'Отменен'
WHERE id = 7;

UPDATE statuses
SET status = 'Завершен'
WHERE id = 8;
