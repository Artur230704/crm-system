create table if not exists roles
(
    id   bigserial
        primary key,
    role varchar(30),
    constraint unique_role unique (role)
);

create table if not exists customers
(
    id            bigserial
        primary key,
    email         varchar,
    company       varchar,
    customer_data varchar,
    enabled       bool default true,
    number        varchar,
    password      varchar,
    post          varchar,
    role_id       bigint
        references roles (id) on delete cascade on update cascade,
    constraint unique_customer_email unique (email),
    constraint unique_customer_number unique (number)
);
create table if not exists employees
(
    id        bigserial
        primary key,
    email     varchar,
    password     varchar,
    full_name varchar,
    inn       varchar,
    enabled       bool default true,
    login     varchar,
    number    varchar,
    post      varchar,
    role_id   bigint references roles(id) on delete cascade on update cascade,
    constraint unique_employee_email unique (email),
    constraint unique_employee_number unique (number),
    constraint unique_employee_inn unique (inn),
    constraint unique_employee_login unique (login)
);
create table if not exists statuses
(
    id     bigserial
        primary key,
    status varchar,
    constraint unique_status unique (status)

);

create table if not exists requests
(
    id                bigserial
        primary key,
    description       varchar,
    other_information varchar,
    product           varchar,
    quantity          integer,
    request_date      timestamp,
    customer_id       bigint
            references customers(id),
    status_id         bigint
            references statuses(id)
);

create table if not exists files
(
    id         bigserial
        primary key,
    file_name  varchar(255),
    request_id bigint
            references requests(id),
    constraint unique_file_in_request unique (request_id,file_name)
);

create table if not exists orders
(
    id          bigserial
        primary key,
    order_date  timestamp,
    customer_id bigint
            references customers(id),
    employee_id bigint
            references employees(id),
    request_id  bigint
            references requests(id),
    status_id   bigint
            references statuses(id)
);

create table if not exists details
(
    id            bigserial
        primary key,
    customs_value real,
    freight       real,
    price         real,
    product_url   varchar,
    quantity      integer,
    order_id      bigint
            references orders(id)
);
