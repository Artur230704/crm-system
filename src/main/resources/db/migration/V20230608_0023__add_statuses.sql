INSERT INTO statuses (status) VALUES ('New');
INSERT INTO statuses (status) VALUES ('In processing');
INSERT INTO statuses (status) VALUES ('Waiting for payment');
INSERT INTO statuses (status) VALUES ('Waiting for confirmation');
INSERT INTO statuses (status) VALUES ('In progress');
INSERT INTO statuses (status) VALUES ('Sent');
INSERT INTO statuses (status) VALUES ('Completed');
INSERT INTO statuses (status) VALUES ('Canceled');
