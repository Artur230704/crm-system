INSERT INTO employees (email, password, full_name, inn, enabled, login, number, post, role_id)
VALUES ('erkinimakeev7@gmail.com', '$2a$12$TpJhTjMFbljvES5iG6/I1ueCFiDJ1JEL/gRz13aa7LvFfnRKDiWKi', 'Erkin Imakeev', '22222222222221', true, 'Erkin', '+996555999999', 'Java Developer', 3),
       ('blindar96@mail.ru', '$2a$12$P.TlELai4t1KJWT3wjg.B.z/N49KSltJdbqWyqiu/AztT/bD3vx/K', 'Vladislav Blindar', '22222222222223', true, 'Vlad', '+996555111111', 'Java Developer', 3),
       ('altynbekov.bai@gmail.com', '$2a$12$lklppJIJCqNCsx7uZRF8Y.J7bwUGOHUW86GMr5cd0eIautG/WZ5ga', 'Orozbay Altynbekov', '22222222222224', true, 'Orozbay', '+996555222222', 'Java Developer', 3),
       ('donkasta07@gmail.com', '$2a$12$0pv.T934AJbg224aptubfuwMbaAweXUNySKySpSmE2jKuejF9EOBa', 'Aidin Almasbekov', '22222222222225', true, 'Aidin', '+996555333333', 'Java Developer', 3),
       ('salmor02@mail.ru', '$2a$12$nUifNPU5uoEVAT0EBFQkle1h5kHk2Kkh/P3JVzaLEWIVdkdg91sjS', 'Salmoorbek Nurzhanov', '22222222222226', true, 'Salmor', '+996555444444', 'Java Developer', 3),
       ('artur230704@gmail.com', '$2a$12$enYoOBhGjWsuYuBdgbjF3eRFUHxhUDb07pDdoKOXRJEX15kYk.Yg.', 'Artur Aidarov', '22222222222227', true, 'Artur', '+996555666666', 'Java Developer', 3),
       ('922688@gmail.com', '$2a$12$AdNaBYIfFf94.vo2a5Ieae8yYT3fl5ZkvT1fyMyG2SeG5arYI1mZ6', 'Dmitriy Frolov', '22222222222228', true, 'Dmitriy', '+996555777777', 'Java Developer', 3);
