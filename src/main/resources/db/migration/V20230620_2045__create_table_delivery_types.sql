create table if not exists delivery_types
(
    id       bigserial primary key,
    delivery varchar
)