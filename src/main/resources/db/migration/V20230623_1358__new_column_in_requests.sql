alter table requests
    add delivery_id bigint references delivery_types (id) on delete cascade on update cascade;