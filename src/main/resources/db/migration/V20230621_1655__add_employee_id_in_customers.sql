alter table customers
    add employee_id bigint references employees(id) on delete cascade on update cascade;