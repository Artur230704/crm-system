#!/bin/bash
set -e

# Меняем настройки max_connections в postgresql.conf
sed -i -e"s/^max_connections = 100.*$/max_connections = 1000/" /var/lib/postgresql/data/postgresql.conf

# Добавляем строку в pg_hba.conf
echo "host all all 0.0.0.0/0 md5" >> /var/lib/postgresql/data/pg_hba.conf

# Запускаем оригинальный entrypoint скрипт
/docker-entrypoint.sh "$@"